package com.estupidyante.battery.components.dashboard.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.estupidyante.battery.R
import com.estupidyante.battery.models.ui.BatteryCard
import java.util.*

class BatteryRVAdapter(batteryCards: ArrayList<BatteryCard>?) :
    RecyclerView.Adapter<BatteryRVAdapter.DashboardViewHolder?>() {
    class DashboardViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var cv: CardView = itemView.findViewById(R.id.cv)
        var icon: ImageView = itemView.findViewById(R.id.icon)
        var label: TextView = itemView.findViewById(R.id.label)
        var value: TextView = itemView.findViewById(R.id.value)
        var indicator: View = itemView.findViewById(R.id.indicator)
    }

    private var mBatteryCards: ArrayList<BatteryCard>?

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DashboardViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(
            R.layout.battery_item_card_view,
            viewGroup,
            false
        )
        return DashboardViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: DashboardViewHolder, i: Int) {
        viewHolder.icon.setImageResource(mBatteryCards!![i].icon)
        viewHolder.label.text = mBatteryCards!![i].label
        viewHolder.value.text = mBatteryCards!![i].value
        viewHolder.indicator.setBackgroundColor(mBatteryCards!![i].indicator)
    }

    fun swap(list: ArrayList<BatteryCard>?) {
        if (mBatteryCards != null) {
            mBatteryCards!!.clear()
            mBatteryCards!!.addAll(list!!)
        } else {
            mBatteryCards = list
        }
        notifyDataSetChanged()
    }

    init {
        mBatteryCards = batteryCards
    }

    override fun getItemCount(): Int {
        return mBatteryCards!!.size
    }
}
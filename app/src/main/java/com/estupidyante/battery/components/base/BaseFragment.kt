package com.estupidyante.battery.components.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    private lateinit var activity: BaseAppCompatActivity

    abstract fun getLayoutId(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is BaseAppCompatActivity) {
            activity = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onFragmentReady(view, savedInstanceState)
    }

    open fun onFragmentReady(view: View, savedInstanceState: Bundle?) {
        // To be implemented by child fragments
    }
}
package com.estupidyante.battery.components.dashboard.statistics

import android.annotation.SuppressLint
import android.content.Context
import android.widget.TextView
import com.estupidyante.battery.R
import com.estupidyante.battery.components.dashboard.adapters.ChartRVAdapter
import com.estupidyante.battery.util.StringHelper
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF

@SuppressLint("ViewConstructor")
class ChartMarkerView(context: Context?, layoutResource: Int, type: Int) : MarkerView(
    context,
    layoutResource
) {
    private var mContent: TextView = findViewById(R.id.tvContent)
    private var mOffset: MPPointF? = null
    private var mType = type

    override fun refreshContent(
        e: Entry,
        highlight: Highlight
    ) {
        var value = ""
        when (mType) {
            ChartRVAdapter.BATTERY_LEVEL -> value = StringHelper.formatPercentageNumber(e.y)
            ChartRVAdapter.BATTERY_TEMPERATURE -> value =
                StringHelper.formatNumber(e.y) + " ºC"
            ChartRVAdapter.BATTERY_VOLTAGE -> value =
                StringHelper.formatNumber(e.y) + " V"
        }
        mContent.text = value

        // this will perform necessary layouting
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        if (mOffset == null) {
            // center the marker horizontally and vertically
            mOffset = MPPointF((-(width shr 1)).toFloat(), (-height).toFloat())
        }
        return mOffset!!
    }
}
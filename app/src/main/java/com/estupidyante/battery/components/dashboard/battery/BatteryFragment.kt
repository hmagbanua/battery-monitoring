package com.estupidyante.battery.components.dashboard.battery

import android.content.Context
import android.graphics.Color
import android.os.BatteryManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.estupidyante.battery.Config
import com.estupidyante.battery.R
import com.estupidyante.battery.components.base.BaseFragment
import com.estupidyante.battery.components.dashboard.DashboardActivity
import com.estupidyante.battery.components.dashboard.adapters.BatteryRVAdapter
import com.estupidyante.battery.events.BatteryLevelEvent
import com.estupidyante.battery.events.BatteryTimeEvent
import com.estupidyante.battery.events.PowerSourceEvent
import com.estupidyante.battery.events.StatusEvent
import com.estupidyante.battery.managers.sampling.DataEstimator
import com.estupidyante.battery.managers.sampling.Inspector
import com.estupidyante.battery.models.Battery
import com.estupidyante.battery.models.ui.BatteryCard
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class BatteryFragment: BaseFragment() {

    private val TAG: String = makeLogTag("HomeFragment")
    private var mContext: Context? = null
    private var mActivity: DashboardActivity? = null
    private var mBatteryPercentage: TextView? = null
    private var mBatteryCurrentNow: TextView? = null
    private var mBatteryCurrentMin: TextView? = null
    private var mBatteryCurrentMax: TextView? = null
    private var mPowerDischarging: ImageView? = null
    private var mPowerAc: ImageView? = null
    private var mPowerUsb: ImageView? = null
    private var mPowerWireless: ImageView? = null
    private var mStatus: TextView? = null
    private var mBatteryCircleBar: ProgressBar? = null
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: BatteryRVAdapter? = null
    private var mBatteryCards: ArrayList<BatteryCard>? = null
    private var mLocalThread: Thread? = null
    private var mHandler: Handler? = null
    private var mMin = 0.0
    private var mMax = 0.0
    private var mActivePower: String? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_battery
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(getLayoutId(), container, false)
        mContext = view.context
        mActivity = activity as DashboardActivity?
        mRecyclerView = view.findViewById(R.id.rv)
        mAdapter = null
        val layout: LinearLayoutManager = object : LinearLayoutManager(mContext) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }

        mRecyclerView?.layoutManager = layout
        mRecyclerView?.setHasFixedSize(true)
        mBatteryPercentage = view.findViewById(R.id.batteryCurrentValue)
        mBatteryCircleBar = view.findViewById(R.id.batteryProgressbar)

        mBatteryCurrentNow = view.findViewById(R.id.batteryCurrentNow)
        mBatteryCurrentMin = view.findViewById(R.id.batteryCurrentMin)
        mBatteryCurrentMax = view.findViewById(R.id.batteryCurrentMax)

        mPowerDischarging = view.findViewById(R.id.imgPowerDischarging)
        mPowerAc = view.findViewById(R.id.imgPowerAc)
        mPowerUsb = view.findViewById(R.id.imgPowerUsb)
        mPowerWireless = view.findViewById(R.id.imgPowerWireless)
        mActivePower = ""
        mMin = Int.MAX_VALUE.toDouble()
        mMax = 0.0
        mHandler = Handler()
        mHandler!!.postDelayed(mRunnable, Config.REFRESH_CURRENT_INTERVAL)
        return view
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        if (mActivity?.getEstimator() != null) {
            val level =
                mActivity!!.getEstimator()?.getLevel().toString()
            mBatteryPercentage!!.text = level
            mBatteryCircleBar!!.progress = mActivity!!.getEstimator()!!.getLevel()
            loadData(mActivity!!.getEstimator()!!)
            loadPluggedState("home")
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun updateBatteryLevelUI(event: BatteryLevelEvent) {
        val text = "" + event.level
        mBatteryPercentage!!.text = text
        mBatteryCircleBar!!.progress = event.level

        // Reload battery cards data from estimator
        loadData(mActivity!!.getEstimator()!!)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun updateBatteryRemainingTime(event: BatteryTimeEvent) {
        // For now, it's just logging
        val logText: String = if (event.charging) {
            "" + event.remainingHours.toString() + " h " +
                    event.remainingMinutes.toString() + " m until complete charge"
        } else {
            "Remaining Time: " + event.remainingHours.toString() + " h " +
                    event.remainingMinutes.toString() + " m"
        }
        LogUtils.logI("BATTERY_LOG", logText)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun updateStatus(event: StatusEvent) {
        mStatus?.setText(event.status)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun updatePowerSource(event: PowerSourceEvent) {
        loadPluggedState(event.status)
        if (mActivePower == "unplugged") {
            resetBatteryCurrent()
        }
    }

    /**
     * Creates an array to feed data to the recyclerView
     *
     * @param estimator Provider of mobile status
     */
    private fun loadData(estimator: DataEstimator) {
        mLocalThread = Thread(Runnable {
            mBatteryCards = ArrayList()
            var value: String?
            var color = Color.GREEN

            // Temperature
            val temperature = estimator.getTemperature()
            value = "$temperature ºC"
            if (temperature > 45) {
                color = Color.RED
            } else if (temperature <= 45 && temperature > 35) {
                color = Color.YELLOW
            }
            mBatteryCards!!.add(
                BatteryCard(
                    R.drawable.ic_thermometer_black_18dp,
                    getString(R.string.battery_summary_temperature),
                    value,
                    color
                )
            )

            // Voltage
            value = estimator.getVoltage().toString() + " V"
            mBatteryCards!!.add(
                BatteryCard(
                    R.drawable.ic_flash_black_18dp,
                    getString(R.string.battery_summary_voltage),
                    value
                )
            )

            // Health
            value = estimator.getHealthStatus(mContext!!)
            color =
                if (value == mContext!!.getString(R.string.battery_health_good)) Color.GREEN else Color.RED
            mBatteryCards!!.add(
                BatteryCard(
                    R.drawable.ic_heart_black_18dp,
                    getString(R.string.battery_summary_health),
                    value!!,
                    color
                )
            )

            // Technology
            if (estimator.getTechnology() == null) {
                color = Color.GRAY
                value = getString(R.string.not_available)
            } else {
                color = if (estimator.getTechnology()
                        .equals("Li-ion")
                ) Color.GRAY else Color.GREEN
                value = estimator.getTechnology()
            }
            mBatteryCards!!.add(
                BatteryCard(
                    R.drawable.ic_wrench_black_18dp,
                    getString(R.string.battery_summary_technology),
                    value!!,
                    color
                )
            )
        })
        mLocalThread!!.start()
        setAdapter()
    }

    private fun setAdapter() {
        try {
            mLocalThread!!.join()
            if (mAdapter == null) {
                mAdapter = BatteryRVAdapter(mBatteryCards)
                mRecyclerView!!.adapter = mAdapter
            } else {
                mAdapter!!.swap(mBatteryCards)
            }
            mRecyclerView!!.invalidate()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    private fun loadPluggedState(status: String) {
        mMin = Int.MAX_VALUE.toDouble()
        mMax = Int.MIN_VALUE.toDouble()
        var batteryCharger = "unplugged"
        if (status == "home") {
            if (mActivity?.getEstimator() == null) return
            when (mActivity!!.getEstimator()!!.getPlugged()) {
                BatteryManager.BATTERY_PLUGGED_AC -> batteryCharger = "ac"
                BatteryManager.BATTERY_PLUGGED_USB -> batteryCharger = "usb"
            }
        } else {
            batteryCharger = status
        }
        when (mActivePower) {
            "unplugged" -> mPowerDischarging!!.setImageResource(R.drawable.ic_battery_50_grey600_24dp)
            "ac" -> mPowerAc!!.setImageResource(R.drawable.ic_power_plug_grey600_24dp)
            "usb" -> mPowerUsb!!.setImageResource(R.drawable.ic_usb_grey600_24dp)
            "wireless" -> mPowerWireless!!.setImageResource(R.drawable.ic_access_point_grey600_24dp)
        }
        when (batteryCharger) {
            "unplugged" -> mPowerDischarging!!.setImageResource(R.drawable.ic_battery_50_white_24dp)
            "ac" -> mPowerAc!!.setImageResource(R.drawable.ic_power_plug_white_24dp)
            "usb" -> mPowerUsb!!.setImageResource(R.drawable.ic_usb_white_24dp)
            "wireless" -> mPowerWireless!!.setImageResource(R.drawable.ic_access_point_white_24dp)
            else -> {
            }
        }
        mActivePower = batteryCharger
    }

    private fun resetBatteryCurrent() {
        mMin = Int.MAX_VALUE.toDouble()
        mMax = 0.0
        var value = "min: --"
        mBatteryCurrentMin!!.text = value
        value = "max: --"
        mBatteryCurrentMax!!.text = value
        value = getString(R.string.battery_measure)
        mBatteryCurrentNow!!.text = value
    }

    private val mRunnable: Runnable = object : Runnable {
        override fun run() {
            val now = Battery.getBatteryCurrentNowInAmperes(mContext!!)
            val level = Inspector.getCurrentBatteryLevel()
            var value: String

            // If is charging and full battery stop mRunnable
            if (mActivePower != "unplugged" && level == 1.0) {
                value = "min: --"
                mBatteryCurrentMin!!.text = value
                value = "max: --"
                mBatteryCurrentMax!!.text = value
                // value = mContext.getString(R.string.battery_full);
                value = String.format(Locale.getDefault(), "%.3f", now) + " A"
                mBatteryCurrentNow!!.text = value
                mHandler!!.postDelayed(this, Config.REFRESH_CURRENT_INTERVAL)
                return
            }
            if (Math.abs(now) < Math.abs(mMin)) {
                mMin = now
                value = "min: " + String.format(
                    Locale.getDefault(),
                    "%.3f",
                    mMin
                ) + " A"
                mBatteryCurrentMin!!.text = value
            }
            if (Math.abs(now) > Math.abs(mMax)) {
                mMax = now
                value = "max: " + String.format(
                    Locale.getDefault(),
                    "%.3f",
                    mMax
                ) + " A"
                mBatteryCurrentMax!!.text = value
            }
            value = String.format(Locale.getDefault(), "%.3f", now) + " A"
            mBatteryCurrentNow!!.text = value
            mHandler!!.postDelayed(this, Config.REFRESH_CURRENT_INTERVAL)
        }
    }
}
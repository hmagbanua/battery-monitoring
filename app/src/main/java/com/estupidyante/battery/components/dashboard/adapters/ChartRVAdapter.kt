package com.estupidyante.battery.components.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.estupidyante.battery.R
import com.estupidyante.battery.components.dashboard.statistics.ChartMarkerView
import com.estupidyante.battery.models.ui.ChartCard
import com.estupidyante.battery.util.DateUtils
import com.estupidyante.battery.util.StringHelper
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.IMarker
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import java.util.*

class ChartRVAdapter(
    private var mChartCards: ArrayList<ChartCard>?,
    private var mInterval: Int,
    private val mContext: Context
) :
    RecyclerView.Adapter<ChartRVAdapter.DashboardViewHolder?>() {

    class DashboardViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var cv: CardView = itemView.findViewById(R.id.cv)
        var label: TextView = itemView.findViewById(R.id.label)
        var interval: TextView = itemView.findViewById(R.id.interval)
        var chart: LineChart = itemView.findViewById(R.id.chart)
        var extras: RelativeLayout = itemView.findViewById(R.id.extra_details)
        var min: TextView = itemView.findViewById(R.id.minValue)
        var avg: TextView = itemView.findViewById(R.id.avgValue)
        var max: TextView = itemView.findViewById(R.id.maxValue)
    }

    fun setInterval(interval: Int) {
        mInterval = interval
    }

    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): DashboardViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.chart_card_view,
            parent,
            false
        )
        return DashboardViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull holder: DashboardViewHolder, position: Int) {
        val card = mChartCards!![position]
        setup(holder, card)
        holder.chart.data = loadData(card)
        holder.chart.invalidate()
        holder.label.text = card.label
        if (card.extras != null) {
            var value: String
            when (card.type) {
                BATTERY_TEMPERATURE -> {
                    value = "Min: " + StringHelper.formatNumber(card.extras!![0]) + " ºC"
                    holder.min.text = value
                    value = mContext.getString(R.string.chart_average_title) +
                            ": " + StringHelper.formatNumber(card.extras!![1]) + " ºC"
                    holder.avg.text = value
                    value = "Max: " + StringHelper.formatNumber(card.extras!![2]) + " ºC"
                    holder.max.text = value
                }
                BATTERY_VOLTAGE -> {
                    value = "Min: " + StringHelper.formatNumber(card.extras!![0]) + " V"
                    holder.min.text = value
                    value = mContext.getString(R.string.chart_average_title) +
                            ": " + StringHelper.formatNumber(card.extras!![1]) + " V"
                    holder.avg.text = value
                    value = "Max: " + StringHelper.formatNumber(card.extras!![2]) + " V"
                    holder.max.text = value
                }
            }
            holder.extras.visibility = View.VISIBLE
        }
        when (mInterval) {
            DateUtils.INTERVAL_24H -> {
                holder.interval.text = "Last 24h"
            }
            DateUtils.INTERVAL_3DAYS -> {
                holder.interval.text = "Last 3 days"
            }
            DateUtils.INTERVAL_5DAYS -> {
                holder.interval.text = "Last 5 days"
            }
        }
    }

    override fun getItemCount(): Int {
        return mChartCards!!.size
    }

    fun swap(list: ArrayList<ChartCard>?) {
        if (mChartCards != null) {
            mChartCards!!.clear()
            mChartCards!!.addAll(list!!)
        } else {
            mChartCards = list
        }
        notifyDataSetChanged()
    }

    private fun loadData(card: ChartCard): LineData {
        // add entries to data set
        val lineDataSet = LineDataSet(card.entries, null)
        lineDataSet.mode = LineDataSet.Mode.LINEAR
        lineDataSet.setDrawValues(false)
        lineDataSet.setDrawCircleHole(false)
        lineDataSet.color = card.color
        lineDataSet.setCircleColor(card.color)
        lineDataSet.lineWidth = 1.8f
        lineDataSet.setDrawFilled(true)
        lineDataSet.fillColor = card.color
        return LineData(lineDataSet)
    }

    private fun setup(holder: DashboardViewHolder, card: ChartCard) {
        holder.chart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return DateUtils.convertMilliSecondsToFormattedDate(value.toLong())
            }
        }
        if (card.type == BATTERY_LEVEL) {
            holder.chart.axisLeft.axisMaximum = 1f
        }
        holder.chart.extraBottomOffset = 5f
        holder.chart.axisLeft.setDrawGridLines(false)
        holder.chart.axisLeft.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return when (card.type) {
                    BATTERY_LEVEL -> StringHelper.formatPercentageNumber(
                        value
                    )
                    BATTERY_TEMPERATURE -> StringHelper.formatNumber(
                        value
                    ) + " ºC"
                    BATTERY_VOLTAGE -> StringHelper.formatNumber(
                        value
                    ) + " V"
                    else -> value.toString()
                }
            }
        }
        holder.chart.axisRight.setDrawGridLines(false)
        holder.chart.axisRight.setDrawLabels(false)
        holder.chart.xAxis.setDrawGridLines(false)
        holder.chart.xAxis.labelCount = 3
        holder.chart.xAxis.granularity = 1f
        holder.chart.legend.isEnabled = false
        holder.chart.description.isEnabled = false
        holder.chart.setNoDataText(mContext.getString(R.string.chart_loading_data))
        val marker: IMarker = ChartMarkerView(
            holder.itemView.context, R.layout.item_marker, card.type
        )
        holder.chart.marker = marker
        holder.chart.animateY(600, Easing.EaseInOutElastic)
    }

    companion object {
        const val BATTERY_LEVEL = 1
        const val BATTERY_TEMPERATURE = 2
        const val BATTERY_VOLTAGE = 3
    }
}
package com.estupidyante.battery.components.dashboard.statistics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.estupidyante.battery.R
import com.estupidyante.battery.components.base.BaseFragment
import com.estupidyante.battery.components.dashboard.DashboardActivity
import com.estupidyante.battery.components.dashboard.adapters.ChartRVAdapter
import com.estupidyante.battery.events.RefreshChartEvent
import com.estupidyante.battery.models.data.BatteryUsage
import com.estupidyante.battery.models.ui.ChartCard
import com.estupidyante.battery.util.DateUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.realm.RealmResults
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class StatisticsFragment: BaseFragment() {
    private val TAG: String = makeLogTag(StatisticsFragment::class.java)

    private var mActivity: DashboardActivity? = null

    private var mRecyclerView: RecyclerView? = null

    private var mAdapter: ChartRVAdapter? = null

    private var mChartCards: ArrayList<ChartCard>? = null

    private var mSelectedInterval = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_statistics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.fragment_statistics, container, false)
        mActivity = activity as DashboardActivity?
        mRecyclerView = view.findViewById(R.id.rv)
        mAdapter = null
        val layout = LinearLayoutManager(view.context)
        mRecyclerView?.layoutManager = layout
        mRecyclerView?.setHasFixedSize(true)

        val bottomNavigationView: BottomNavigationView = view.findViewById(R.id.bottom_navigation)

        bottomNavigationView.setOnNavigationItemSelectedListener(
            object : BottomNavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
                    when (item.itemId) {
                        R.id.action_24h -> {
                            mSelectedInterval = DateUtils.INTERVAL_24H
                            loadData(DateUtils.INTERVAL_24H)
                            return true
                        }
                        R.id.action_3days -> {
                            mSelectedInterval = DateUtils.INTERVAL_3DAYS
                            loadData(DateUtils.INTERVAL_3DAYS)
                            return true
                        }
                        R.id.action_5days -> {
                            mSelectedInterval = DateUtils.INTERVAL_5DAYS
                            loadData(DateUtils.INTERVAL_5DAYS)
                            return true
                        }
                    }
                    return false
                }
            })

        mSelectedInterval = DateUtils.INTERVAL_24H

        return view
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        loadData(mSelectedInterval)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun refreshChartsData(event: RefreshChartEvent?) {
        loadData(mSelectedInterval)
    }

    /**
     * Queries the data from mDatabase.
     *
     * @param interval Time interval for fetching the data.
     */
    private fun loadData(interval: Int) {
        val now = System.currentTimeMillis()
        val results: RealmResults<BatteryUsage>
        mChartCards = ArrayList<ChartCard>()

        // Make sure mDatabase instance is opened
        if (mActivity!!.mDatabase!!.isClosed) {
            mActivity!!.mDatabase?.defaultInstance
        }

        // Query results according to selected time interval
        results = when (interval) {
            DateUtils.INTERVAL_3DAYS -> {
                mActivity?.mDatabase!!.betweenUsages(
                    DateUtils.getMilliSecondsInterval(DateUtils.INTERVAL_3DAYS),
                    now
                )
            }
            DateUtils.INTERVAL_5DAYS -> {
                mActivity?.mDatabase!!.betweenUsages(
                    DateUtils.getMilliSecondsInterval(DateUtils.INTERVAL_5DAYS),
                    now
                )
            }
            else -> {
                mActivity?.mDatabase!!.betweenUsages(
                    DateUtils.getMilliSecondsInterval(DateUtils.INTERVAL_24H),
                    now
                )
            }
        }
        fillData(results)
        setAdapter(mSelectedInterval)
    }

    /**
     * Fills in the data queried from the mDatabase.
     *
     * @param results Collection of results fetched from the mDatabase.
     */
    private fun fillData(@NonNull results: RealmResults<BatteryUsage>) {
        var min = 0.0
        var avg = 0.0
        var max = 0.0

        // Battery Level
        var card: ChartCard = ChartCard(
            ChartRVAdapter.BATTERY_LEVEL,
            getString(R.string.chart_battery_level),
            ColorTemplate.rgb("#E84813")
        )

        for (usage in results) {
            card.entries.plus(
                Entry(
                    usage.timestamp.toFloat(),
                    usage.level
                )
            )
        }
        mChartCards!!.add(card)

        // Battery Temperature
        if (!results.isEmpty()) {
            min = Double.MAX_VALUE
            avg = 0.0
            max = Double.MIN_VALUE
        }
        card = ChartCard(
            ChartRVAdapter.BATTERY_TEMPERATURE,
            getString(R.string.chart_battery_temperature),
            ColorTemplate.rgb("#E81332")
        )
        for (usage in results) {
            card.entries.plus(
                Entry(
                    usage.timestamp.toFloat(),
                    usage.details!!.temperature.toFloat()
                )
            )
            if (usage.details!!.temperature < min) {
                min = usage.details!!.temperature
            }
            if (usage.details!!.temperature > max) {
                max = usage.details!!.temperature
            }
            avg += usage.details!!.temperature
        }
        avg /= results.size.toDouble()
        card.extras = doubleArrayOf(min, avg, max)
        mChartCards!!.add(card)

        // Battery Voltage
        if (!results.isEmpty()) {
            min = Double.MAX_VALUE
            avg = 0.0
            max = Double.MIN_VALUE
        }
        card = ChartCard(
            ChartRVAdapter.BATTERY_VOLTAGE,
            getString(R.string.chart_battery_voltage),
            ColorTemplate.rgb("#FF15AC")
        )
        for (usage in results) {
            card.entries.plus(
                Entry(
                    usage.timestamp.toFloat(),
                    usage.details!!.voltage.toFloat()
                )
            )
            if (usage.details!!.voltage < min) {
                min = usage.details!!.voltage
            }
            if (usage.details!!.voltage > max) {
                max = usage.details!!.voltage
            }
            avg += usage.details!!.voltage
        }
        avg /= results.size.toDouble()
        card.extras = doubleArrayOf(min, avg, max)
        mChartCards!!.add(card)
    }

    /**
     * Sets the adapter of the recycler view,
     * filtering the time interval of the charts.
     *
     * @param interval Time interval to filter charts results.
     */
    private fun setAdapter(interval: Int) {
        if (mAdapter == null) {
            // We need the application context to access String resources within the Adapter
            val context = activity!!.applicationContext
            mAdapter = ChartRVAdapter(mChartCards, interval, context)
            mRecyclerView?.setAdapter(mAdapter)
        } else {
            mAdapter?.setInterval(interval)
            mAdapter?.swap(mChartCards)
        }
        mRecyclerView?.invalidate()
    }
}
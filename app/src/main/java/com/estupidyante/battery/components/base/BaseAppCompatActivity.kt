package com.estupidyante.battery.components.base

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.estupidyante.battery.R

abstract class BaseAppCompatActivity : AppCompatActivity(),
    OnSharedPreferenceChangeListener {
    // Primary toolbar
    private var mActionBarToolbar: Toolbar? = null

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sp =
            PreferenceManager.getDefaultSharedPreferences(this)
        sp.registerOnSharedPreferenceChangeListener(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        actionBarToolbar
    }

    private val actionBarToolbar: Toolbar?
        get() {
            if (mActionBarToolbar == null) {
                mActionBarToolbar = findViewById(R.id.toolbar_actionbar)
                if (mActionBarToolbar != null) {
                    setSupportActionBar(mActionBarToolbar)
                }
            }
            return mActionBarToolbar
        }

    override fun onDestroy() {
        super.onDestroy()
        val sp =
            PreferenceManager.getDefaultSharedPreferences(this)
        sp.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(
        sharedPreferences: SharedPreferences,
        key: String
    ) {
        // this is empty
    }
}
package com.estupidyante.battery.components.dashboard

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.viewpager.widget.ViewPager
import com.estupidyante.battery.BatteryApp
import com.estupidyante.battery.Config
import com.estupidyante.battery.R
import com.estupidyante.battery.components.base.BaseAppCompatActivity
import com.estupidyante.battery.components.dashboard.adapters.DashboardPagerAdapter
import com.estupidyante.battery.components.dashboard.battery.BatteryFragment
import com.estupidyante.battery.components.dashboard.statistics.StatisticsFragment
import com.estupidyante.battery.managers.sampling.DataEstimator
import com.estupidyante.battery.managers.storage.BatteryAppDb
import com.estupidyante.battery.models.Sensors
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseAppCompatActivity(), Toolbar.OnMenuItemClickListener,
    ActivityCompat.OnRequestPermissionsResultCallback, SensorEventListener {

    private val TAG: String = makeLogTag(DashboardActivity::class.java)

    private val tabIcons =
        intArrayOf(R.drawable.ic_battery_50_white_24dp, R.drawable.ic_chart_areaspline_white_24dp)
    private lateinit var adapter: DashboardPagerAdapter

    private var mApp: BatteryApp? = null
    internal var mDatabase: BatteryAppDb? = null

    private var mViewPager: ViewPager? = null
    private var mSensorManager: SensorManager? = null
    private var mSensorList: List<Sensor>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        LogUtils.logI(TAG, "onCreate() called")

        val intentFromNotifier: Intent = intent
        val tab = intentFromNotifier.getIntExtra("tab", -1)
        if (tab != -1) {
            mViewPager?.currentItem = tab
        }
        mSensorManager =
            baseContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager?
        mSensorList = mSensorManager!!.getSensorList(Sensor.TYPE_ALL)
    }

    override fun onStart() {
        super.onStart()
        loadComponents()
        mDatabase?.defaultInstance
        for (sensor in mSensorList!!) {
            mSensorManager!!.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    private fun initializeToolbar() {
        setSupportActionBar(activityDashboardToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun initializeAdapter() {
        adapter = DashboardPagerAdapter(supportFragmentManager)
        adapter.addFragment(
            BatteryFragment())

        adapter.addFragment(
            StatisticsFragment())

        activityDashboardViewPager.adapter = adapter
        setupPermission(
            Manifest.permission.READ_PHONE_STATE,
            Config.PERMISSION_READ_PHONE_STATE
        )
    }

    private fun initializeTabs() {
        activityDashboardTabLayout.setupWithViewPager(activityDashboardViewPager)
        activityDashboardTabLayout.getTabAt(0)?.setIcon(tabIcons[0])
        activityDashboardTabLayout.getTabAt(1)?.setIcon(tabIcons[1])
    }

    override fun onStop() {
        mSensorManager!!.unregisterListener(this)
        mDatabase?.close()
        super.onStop()
    }

    override fun onAccuracyChanged(
        sensor: Sensor,
        accuracy: Int
    ) {
        // Do something here if sensor accuracy changes.
    }

    override fun onSensorChanged(event: SensorEvent) {
        Sensors.onSensorChanged(event)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        return true
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        // Do something here if sensor accuracy changes.
        return false
    }

    private fun setupPermission(permission: String, code: Int) {
        if (checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(
                this, arrayOf(permission),
                code
            )
        }
    }

    fun getEstimator(): DataEstimator? {
        return mApp?.estimator
    }

    private fun loadComponents() {
        mDatabase = BatteryAppDb()
        mApp = BatteryApp()

        mApp?.startBatteryService()

        initializeToolbar()
        initializeAdapter()
        initializeTabs()
    }

    companion object {
        fun onRequestPermissionsResult(
            dashboardActivity: DashboardActivity, requestCode: Int,
            @NonNull permissions: Array<String?>?,
            @NonNull grantResults: IntArray?
        ) {
            when (requestCode) {
                Config.PERMISSION_READ_PHONE_STATE -> {

                    // If request is cancelled, the result arrays are empty.
                    dashboardActivity.setupPermission(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Config.PERMISSION_ACCESS_COARSE_LOCATION
                    )
                }
                Config.PERMISSION_ACCESS_COARSE_LOCATION -> {
                    dashboardActivity.setupPermission(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Config.PERMISSION_ACCESS_FINE_LOCATION
                    )
                }
            }
        }
    }
}
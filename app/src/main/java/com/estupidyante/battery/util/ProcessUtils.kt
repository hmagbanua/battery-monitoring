package com.estupidyante.battery.util

import com.estupidyante.battery.managers.sampling.Inspector
import com.estupidyante.battery.util.LogUtils.logE
import com.estupidyante.battery.util.LogUtils.makeLogTag
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

object ProcessUtils {
    private val TAG: String = makeLogTag(Inspector::class.java)
    fun getCommandOutputAsList(command: String?): List<String> {
        val lines = getCommandOutput(command)
            .split(System.getProperty("line.separator")!!).toTypedArray()
        return ArrayList(listOf(*lines))
    }

    private fun getCommandOutput(command: String?): String {
        try {
            // Executes the command.
            val process = Runtime.getRuntime().exec(command)

            // Reads stdout.
            // NOTE: You can write to stdin of the command using
            //       process.getOutputStream().
            val reader = BufferedReader(
                InputStreamReader(process.inputStream)
            )
            var read: Int
            val buffer = CharArray(4096)
            val output = StringBuffer()
            while (reader.read(buffer).also { read = it } > 0) {
                output.append(buffer, 0, read)
            }
            reader.close()

            // Waits for the command to finish.
            process.waitFor()
            return output.toString()
        } catch (e: IOException) {
            logE(TAG, "Could not get ps command output")
        } catch (e: InterruptedException) {
            logE(TAG, "Could not get ps command output")
        }
        return ""
    }
}
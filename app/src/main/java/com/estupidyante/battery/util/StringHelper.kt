package com.estupidyante.battery.util

import android.app.ActivityManager.RunningAppProcessInfo
import android.content.Context
import android.util.SparseArray
import com.estupidyante.battery.Config
import com.estupidyante.battery.R
import java.text.NumberFormat

object StringHelper {
    // Used to map importances to human readable strings for sending samples to
    // the server, and showing them in the process list.
    private var importanceToString: SparseArray<String>? = null

    /**
     * Converts `importance` to a human readable string.
     *
     * @param importance the importance from Android process info.
     * @return a human readable String describing the importance.
     */
    fun importanceString(importance: Int): String {
        var s = importanceToString!![importance]
        if (s == null || s.length == 0) {
            LogUtils.logE("Importance not found: ", "" + importance)
            s = "Unknown"
        }
        return s
    }

    fun importanceStringLegacy(description: String?): String {
        val importance: String = when (description) {
            "system" -> importanceString(RunningAppProcessInfo.IMPORTANCE_BACKGROUND)
            "user" -> importanceString(Config.IMPORTANCE_APP)
            "user-service" -> importanceString(RunningAppProcessInfo.IMPORTANCE_SERVICE)
            else -> "Unknown"
        }
        return importance
    }

    fun translatedPriority(
        context: Context,
        importanceString: String?
    ): String {
        return if (importanceString == null) {
            context.getString(R.string.priorityDefault)
        } else when (importanceString) {
            "Not running" -> context.getString(R.string.prioritynotrunning)
            "Background process" -> context.getString(R.string.prioritybackground)
            "Service" -> context.getString(R.string.priorityservice)
            "Visible task" -> context.getString(R.string.priorityvisible)
            "Foreground app" -> context.getString(R.string.priorityforeground)
            "Perceptible task" -> context.getString(R.string.priorityperceptible)
            "Suggestion" -> context.getString(R.string.prioritysuggestion)
            else -> context.getString(R.string.priorityDefault)
        }
    }

    fun formatProcessName(processName: String): String {
        var indexOf = processName.lastIndexOf(':')
        if (indexOf <= 0) indexOf = processName.length
        return processName.substring(0, indexOf)
    }

    fun formatPercentageNumber(number: Float): String {
        val defaultFormat = NumberFormat.getPercentInstance()
        defaultFormat.minimumFractionDigits = 0
        return defaultFormat.format(number.toDouble())
    }

    fun formatNumber(number: Float): String {
        val defaultFormat = NumberFormat.getNumberInstance()
        defaultFormat.minimumFractionDigits = 1
        return defaultFormat.format(number.toDouble())
    }

    fun formatNumber(number: Double): String {
        val defaultFormat = NumberFormat.getNumberInstance()
        defaultFormat.maximumFractionDigits = 2
        return defaultFormat.format(number)
    }

    fun trimArray(array: Array<String>): Array<String> {
        for (i in array.indices) {
            array[i] = array[i].trim { it <= ' ' }
        }
        return array
    }

    fun convertToString(obj: Any): String {
        if (obj is List<*>) {
            val s = obj.toString()
            // remove '[' and ']' chars from List.toString()
            return s.substring(1, s.length - 1)
        }
        return obj.toString()
    }

    fun truncate(str: String, len: Int): String {
        return if (str.length > len) {
            str.substring(0, len) + "..."
        } else {
            str
        }
    }

    init {
        importanceToString = SparseArray()
        // for API >=  26, both IMPORTANCE_EMPTY and IMPORTANCE_BACKGROUND
        // will mean the same thing, and the constant name is IMPORTANCE_CACHED
        importanceToString!!.put(RunningAppProcessInfo.IMPORTANCE_CACHED, "Not running")
        importanceToString!!.put(RunningAppProcessInfo.IMPORTANCE_EMPTY, "Not running")
        importanceToString!!.put(
            RunningAppProcessInfo.IMPORTANCE_BACKGROUND,
            "Background process"
        )
        importanceToString!!.put(RunningAppProcessInfo.IMPORTANCE_SERVICE, "Service")
        importanceToString!!.put(
            RunningAppProcessInfo.IMPORTANCE_VISIBLE,
            "Visible task"
        )
        importanceToString!!.put(
            RunningAppProcessInfo.IMPORTANCE_FOREGROUND,
            "Foreground app"
        )
        importanceToString!!.put(Config.IMPORTANCE_APP, "App")
    }
}
package com.estupidyante.battery.util

import android.util.Log


object LogUtils {
    private const val LOG_PREFIX = "battery_monitor_"
    private const val LOG_PREFIX_LENGTH = LOG_PREFIX.length
    private const val MAX_LOG_TAG_LENGTH = 23
    var LOGGING_ENABLED = false

    fun makeLogTag(str: String): String {
        return if (str.length > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            LOG_PREFIX + str.substring(
                0,
                MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1
            )
        } else LOG_PREFIX + str
    }

    /**
     * Don't use this when obfuscating class names!
     */
    fun makeLogTag(cls: Class<*>): String {
        return makeLogTag(cls.simpleName)
    }

    fun logD(tag: String?, message: String?) {
        if (LOGGING_ENABLED && message != null && Log.isLoggable(
                tag,
                Log.DEBUG
            )
        ) {
            Log.d(tag, message)
        }
    }

    fun logD(tag: String?, message: String?, cause: Throwable?) {
        if (LOGGING_ENABLED && message != null && Log.isLoggable(
                tag,
                Log.DEBUG
            )
        ) {
            Log.d(tag, message, cause)
        }
    }

    fun logI(tag: String?, message: String?) {
        if (LOGGING_ENABLED && message != null) {
            Log.i(tag, message)
        }
    }

    fun logI(tag: String?, message: String?, cause: Throwable?) {
        if (LOGGING_ENABLED && message != null) {
            Log.i(tag, message, cause)
        }
    }

    fun logW(tag: String?, message: String?) {
        if (LOGGING_ENABLED && message != null) {
            Log.w(tag, message)
        }
    }

    fun logW(tag: String?, message: String?, cause: Throwable?) {
        if (LOGGING_ENABLED && message != null) {
            Log.w(tag, message, cause)
        }
    }

    fun logE(tag: String?, message: String?) {
        if (LOGGING_ENABLED && message != null) {
            Log.e(tag, message)
        }
    }

    fun logE(tag: String?, message: String?, cause: Throwable?) {
        if (LOGGING_ENABLED && message != null) {
            Log.e(tag, message, cause)
        }
    }
}
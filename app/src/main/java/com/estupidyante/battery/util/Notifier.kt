package com.estupidyante.battery.util

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.estupidyante.battery.Config
import com.estupidyante.battery.R
import com.estupidyante.battery.components.dashboard.DashboardActivity
import com.estupidyante.battery.managers.sampling.DataEstimator
import com.estupidyante.battery.managers.sampling.Inspector
import com.estupidyante.battery.models.Battery

object Notifier {
    private val TAG = LogUtils.makeLogTag(Notifier::class.java)
    private var isStatusBarShown = false
    private var sBuilder: NotificationCompat.Builder? = null
    private var sNotificationManager: NotificationManager? = null
    fun startStatusBar(context: Context) {
        if (isStatusBarShown) return

        val estimator = DataEstimator
        estimator.getCurrentStatus(context)
        val now: Double = Battery.getBatteryCurrentNow(context).toDouble()
        val level: Int = estimator.getLevel()
        val title = context.getString(R.string.now) + ": " + now + " mA"
        val text = context.getString(R.string.notif_batteryhub_running)
        sBuilder = NotificationCompat.Builder(context, "start_status")
            .setContentTitle(title)
            .setContentText(text)
            .setAutoCancel(false)
            .setOngoing(true)
            .setPriority(SettingsUtils.fetchNotificationsPriority(context))
        sBuilder!!.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        if (level < 100) {
            sBuilder!!.setSmallIcon(R.drawable.ic_stat_00_pct_charged + level)
        } else {
            sBuilder!!.setSmallIcon(R.drawable.ic_stat_z100_pct_charged)
        }

        // Creates an explicit intent for an Activity in your app
        val resultIntent = Intent(context, DashboardActivity::class.java)

        val stackBuilder =
            TaskStackBuilder.create(context)
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(DashboardActivity::class.java)
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(
            0,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        sBuilder?.setContentIntent(resultPendingIntent)
        sNotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // mId allows you to update the notification later on.
        sNotificationManager!!.notify(
            Config.NOTIFICATION_BATTERY_STATUS,
            sBuilder?.build()
        )
        isStatusBarShown = true
    }

    fun updateStatusBar(context: Context) {
        // In case status bar is not started yet call start method
        if (!isStatusBarShown) {
            startStatusBar(context)
            return
        }
        val now: Double = Battery.getBatteryCurrentNow(context).toDouble()
        val level = (Inspector.getCurrentBatteryLevel() * 100) as Int
        val title = context.getString(R.string.now) + ": " + now + " mA"
        val text = context.getString(R.string.notif_batteryhub_running)
        sBuilder!!.setContentTitle(title)
            .setContentText(text)
            .setAutoCancel(false)
            .setOngoing(true).priority = SettingsUtils.fetchNotificationsPriority(context)
        sBuilder?.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        if (level < 100) {
            sBuilder?.setSmallIcon(R.drawable.ic_stat_00_pct_charged + level)
        } else {
            sBuilder?.setSmallIcon(R.drawable.ic_stat_z100_pct_charged)
        }
        LogUtils.logI(TAG, "Updating value of notification")

        // Because the ID remains unchanged, the existing notification is updated.
        sNotificationManager!!.notify(
            Config.NOTIFICATION_BATTERY_STATUS,
            sBuilder?.build()
        )
    }

    fun closeStatusBar() {
        sNotificationManager!!.cancel(Config.NOTIFICATION_BATTERY_STATUS)
        isStatusBarShown = false
    }
}
package com.estupidyante.battery.util

import android.content.Context
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.preference.PreferenceManager
import com.estupidyante.battery.Config
import io.fabric.sdk.android.BuildConfig

object SettingsUtils {
    private const val TAG = "SettingsUtils"
    const val PREF_TOS_ACCEPTED = "pref_tos_accepted"
    const val PREF_DEVICE_REGISTERED = "pref_device_registered"
    const val PREF_SERVER_URL = "pref_server_url"
    const val PREF_SEND_INSTALLED_PACKAGES = "pref_send_installed"
    const val PREF_SAMPLING_SCREEN = "pref_sampling_screen"
    const val PREF_DATA_HISTORY = "pref_data_history"
    const val PREF_MOBILE_DATA = "pref_mobile_data"
    const val PREF_AUTO_UPLOAD = "pref_auto_upload"
    const val PREF_UPLOAD_RATE = "pref_upload_rate"
    const val PREF_NOTIFICATIONS_PRIORITY = "pref_notifications_priority"
    const val PREF_POWER_INDICATOR = "pref_power_indicator"
    const val PREF_BATTERY_ALERTS = "pref_battery_alerts"
    const val PREF_CHARGE_ALERTS = "pref_charge_alerts"
    const val PREF_TEMPERATURE_ALERTS = "pref_temperature_alerts"
    const val PREF_TEMPERATURE_RATE = "pref_temperature_rate"
    const val PREF_LAST_TEMPERATURE_ALERT = "pref_last_temperature_alert"
    const val PREF_TEMPERATURE_WARNING = "pref_temperature_warning"
    const val PREF_TEMPERATURE_HIGH = "pref_temperature_high"
    const val PREF_MESSAGE_ALERTS = "pref_message_alerts"
    const val PREF_APP_VERSION = "pref_app_version"
    const val PREF_MESSAGE_LAST_ID = "pref_message_last"
    const val PREF_HIDE_SYSTEM_APPS = "pref_system_apps"
    const val PREF_USE_OLD_MEASUREMENT = "pref_old_measurement"
    const val PREF_REMAINING_TIME = "pref_remaining_time"

    /**
     * Return true if user has accepted the
     * [Tos][WelcomeActivity], false if they haven't (yet).
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isTosAccepted(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_TOS_ACCEPTED, false)
    }

    /**
     * Mark `newValue whether` the user has accepted the TOS so the app doesn't ask again.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param newValue New value that will be set.
     */
    fun markTosAccepted(context: Context?, newValue: Boolean) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putBoolean(PREF_TOS_ACCEPTED, newValue).apply()
    }

    /**
     * Mark `newValue whether` the device has registered in the web server
     * so the app doesn't register again.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param newValue New value that will be set.
     */
    fun markDeviceAccepted(
        context: Context?,
        newValue: Boolean
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putBoolean(PREF_DEVICE_REGISTERED, newValue).apply()
    }

    /**
     * Return true if device has been registered in the web server,
     * false if they haven't (yet).
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isDeviceRegistered(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_DEVICE_REGISTERED, false)
    }

    /**
     * Save `url` of the web server.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param url New value that will be set.
     */
    fun saveServerUrl(context: Context?, url: String?) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(PREF_SERVER_URL, url).apply()
    }

    /**
     * Fetch stored `url` of the web server.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     */
    fun fetchServerUrl(context: Context?): String? {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(PREF_SERVER_URL, Config.SERVER_URL_DEFAULT)
    }

    fun isServerUrlPresent(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(PREF_SERVER_URL, Config.SERVER_URL_DEFAULT) != Config.SERVER_URL_DEFAULT
    }

    fun markInstalledPackagesIncluded(
        context: Context?,
        newValue: Boolean
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putBoolean(PREF_SEND_INSTALLED_PACKAGES, newValue).apply()
    }

    fun isInstalledPackagesIncluded(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_SEND_INSTALLED_PACKAGES, false)
    }

    /**
     * Return true if saving data for screen on/off is allowed, false if it is not.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isSamplingScreenOn(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_SAMPLING_SCREEN, false)
    }

    /**
     * Return true if mobile data is allowed to upload samples,
     * false if it is not.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isMobileDataAllowed(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_MOBILE_DATA, false)
    }

    /**
     * Return true if automatic uploading is allowed,
     * false if it is not.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isAutomaticUploadingAllowed(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_AUTO_UPLOAD, true)
    }

    /**
     * Return true if power indicator is to be shown, false if it is hidden.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isPowerIndicatorShown(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_POWER_INDICATOR, false)
    }

    /**
     * Return true if battery alerts are to be shown, false if hidden.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isBatteryAlertsOn(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_BATTERY_ALERTS, true)
    }

    /**
     * Return true if charge related alerts are to be shown, false if hidden.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isChargeAlertsOn(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_CHARGE_ALERTS, true)
    }

    /**
     * Return true if temperature related alerts are to be shown, false if hidden.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isTemperatureAlertsOn(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_TEMPERATURE_ALERTS, true)
    }

    /**
     * Save the new last time of battery temperature alert.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param time New value that will be set.
     */
    fun saveLastTemperatureAlertDate(
        context: Context?,
        time: Long
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putLong(PREF_LAST_TEMPERATURE_ALERT, time).apply()
    }

    /**
     * Return the time in millis of the last battery temperature alert
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun fetchLastTemperatureAlertDate(context: Context?): Long {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getLong(PREF_LAST_TEMPERATURE_ALERT, 0)
    }

    /**
     * Return true if message alerts are to be shown, false if hidden.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isMessageAlertsOn(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_MESSAGE_ALERTS, true)
    }

    /**
     * Save the message `id`.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param id New value that will be set.
     */
    fun saveLastMessageId(context: Context?, id: Int) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putInt(PREF_MESSAGE_LAST_ID, id).apply()
    }

    /**
     * Fetch the last message `id`.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     */
    fun fetchLastMessageId(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getInt(PREF_MESSAGE_LAST_ID, Config.STARTER_MESSAGE_ID)
    }

    /**
     * Return true if system apps are hidden
     * [Setting][TaskListActivity], false if they aren't (yet).
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isSystemAppsHidden(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_HIDE_SYSTEM_APPS, true)
    }

    /**
     * Mark `newValue whether` system apps to be hidden from list.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param newValue New value that will be set.
     */
    fun markSystemAppsHidden(
        context: Context?,
        newValue: Boolean
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putBoolean(PREF_HIDE_SYSTEM_APPS, newValue).apply()
    }

    /**
     * Return true if old measurement method is being used, false if it isn't.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isOldMeasurementUsed(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_USE_OLD_MEASUREMENT, false)
    }

    /**
     * Mark `newValue whether` old measurement method to be used.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param newValue New value that will be set.
     */
    fun markOldMeasurementUsed(
        context: Context?,
        newValue: Boolean
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putBoolean(PREF_USE_OLD_MEASUREMENT, newValue).apply()
    }

    /**
     * Save the most recent app version `version`.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     * @param version New value that will be set.
     */
    fun saveAppVersion(context: Context?, version: Int) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putInt(PREF_APP_VERSION, version).apply()
    }

    /**
     * Fetch the most recent app version `version`.
     *
     * @param context Context to be used to edit the [android.content.SharedPreferences].
     */
    fun fetchAppVersion(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getInt(PREF_APP_VERSION, BuildConfig.VERSION_CODE)
    }

    /**
     * Return true if remaining time alert is to display, false if it isn't.
     *
     * @param context Context to be used to lookup the [android.content.SharedPreferences].
     */
    fun isRemainingTimeAlertOn(context: Context?): Boolean {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(PREF_USE_OLD_MEASUREMENT, false)
    }
    // region Listeners
    /**
     * Helper method to register a settings_prefs listener. This method does not
     * automatically handle `unregisterOnSharedPreferenceChangeListener() un-registering`
     * the listener at the end of the `context` lifecycle.
     *
     * @param context  Context to be used to lookup the [android.content.SharedPreferences].
     * @param listener Listener to register.
     */
    fun registerOnSharedPreferenceChangeListener(
        context: Context?,
        listener: OnSharedPreferenceChangeListener?
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.registerOnSharedPreferenceChangeListener(listener)
    }

    /**
     * Helper method to un-register a settings_prefs listener typically registered with
     * `registerOnSharedPreferenceChangeListener()`
     *
     * @param context  Context to be used to lookup the [android.content.SharedPreferences].
     * @param listener Listener to un-register.
     */
    fun unregisterOnSharedPreferenceChangeListener(
        context: Context?,
        listener: OnSharedPreferenceChangeListener?
    ) {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        sp.unregisterOnSharedPreferenceChangeListener(listener)
    }

    fun fetchTemperatureWarning(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(
            PREF_TEMPERATURE_WARNING,
            Config.NOTIFICATION_DEFAULT_TEMPERATURE_WARNING
        )!!.replaceFirst("^0+(?!$)".toRegex(), "")
            .toInt()
    }

    fun fetchTemperatureAlertsRate(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(
            PREF_TEMPERATURE_RATE,
            Config.NOTIFICATION_DEFAULT_TEMPERATURE_RATE
        )!!.toInt()
    }

    fun fetchTemperatureHigh(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(
            PREF_TEMPERATURE_HIGH,
            Config.NOTIFICATION_DEFAULT_TEMPERATURE_HIGH
        )!!.replaceFirst("^0+(?!$)".toRegex(), "")
            .toInt()
    }

    fun fetchDataHistoryInterval(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(PREF_DATA_HISTORY, Config.DATA_HISTORY_DEFAULT)!!
            .toInt()
    }

    fun fetchNotificationsPriority(context: Context?): Int {
        val sp =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(
            PREF_NOTIFICATIONS_PRIORITY,
            Config.NOTIFICATION_DEFAULT_PRIORITY
        )!!.toInt()
    }
}
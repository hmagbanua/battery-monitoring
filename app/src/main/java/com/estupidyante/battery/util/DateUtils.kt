package com.estupidyante.battery.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    const val INTERVAL_24H = 1
    const val INTERVAL_3DAYS = 2
    const val INTERVAL_5DAYS = 3
    const val INTERVAL_10DAYS = 4
    const val INTERVAL_15DAYS = 5
    private const val DATE_FORMAT = "dd-MM HH:mm"
    private val sSimpleDateFormat =
        SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

    fun convertMilliSecondsToFormattedDate(milliSeconds: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return sSimpleDateFormat.format(calendar.time)
    }

    fun getMilliSecondsInterval(interval: Int): Long {
        val now = System.currentTimeMillis()
        when (interval) {
            INTERVAL_24H -> {
                return now - 1000 * 60 * 60 * 24
            }
            INTERVAL_3DAYS -> {
                return now - 1000 * 60 * 60 * 24 * 3
            }
            INTERVAL_5DAYS -> {
                return now - 1000 * 60 * 60 * 24 * 5
            }
            INTERVAL_10DAYS -> {
                return now - 1000 * 60 * 60 * 24 * 10
            }
            INTERVAL_15DAYS -> {
                return now - 1000 * 60 * 60 * 24 * 15
            }
            else -> return now
        }
    }
}
package com.estupidyante.battery.models

import android.os.SystemClock
import com.estupidyante.battery.util.LogUtils.makeLogTag
import java.io.IOException
import java.io.RandomAccessFile
import java.util.concurrent.TimeUnit

object Cpu {
    private val TAG: String = makeLogTag(Cpu::class.java)

    @Synchronized
    fun readUsagePoint(): LongArray {
        var idle: Long = 0
        var cpu: Long = 0
        try {
            val reader = RandomAccessFile("/proc/stat", "r")
            val load = reader.readLine()
            val tokens = load.split(" ").toTypedArray()
            for (i in 2..8) {
                // 5 index has idle value
                if (i == 5) {
                    idle = tokens[i].toLong()
                    continue
                }
                cpu += tokens[i].toLong()
            }
            reader.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return longArrayOf(idle, cpu)
    }

    fun getUsage(then: LongArray?, now: LongArray?): Double {
        if (then == null || now == null || then.size < 2 || now.size < 2) return 0.0
        val idleAndCpuDiff =
            now[0] + now[1] - (then[0] + then[1]).toDouble()
        return (now[1] - then[1]) / idleAndCpuDiff
    }

    val uptime: Long
        get() = TimeUnit.MILLISECONDS.toSeconds(SystemClock.elapsedRealtime())

    val sleepTime: Long
        get() {
            val sleep =
                SystemClock.elapsedRealtime() - SystemClock.uptimeMillis()
            return if (sleep < 0) 0 else TimeUnit.MILLISECONDS.toSeconds(sleep)
        }
}
package com.estupidyante.battery.models

import android.content.Context
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException

object Screen {
    fun getBrightness(context: Context): Int {
        var screenBrightnessValue = 0
        try {
            screenBrightnessValue = Settings.System.getInt(
                context.contentResolver,
                Settings.System.SCREEN_BRIGHTNESS
            )
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
        }
        return screenBrightnessValue
    }

    fun isAutoBrightness(context: Context): Boolean {
        var autoBrightness = false
        try {
            val brightnessMode = Settings.System.getInt(
                context.contentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE
            )
            autoBrightness =
                brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
        }
        return autoBrightness
    }

    fun isOn(context: Context): Int {
        val powerManager =
            context.getSystemService(Context.POWER_SERVICE) as PowerManager
                ?: return 0
        return if (Build.VERSION.SDK_INT >= 20) {
            if (powerManager.isInteractive) 1 else 0
        } else {
            if (powerManager.isScreenOn) 1 else 0
        }
    }
}
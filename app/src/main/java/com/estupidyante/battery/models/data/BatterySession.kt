package com.estupidyante.battery.models.data

import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class BatterySession : RealmObject() {
    @PrimaryKey
    var id = 0

    // Timestamp of session
    @Index
    var timestamp: Long = 0

    // Level of the battery (0 - 1.0) translates to percentage
    var level = 0f

    // If screen is on == 1, off == 0
    var screenOn = 0

    // Trigger reason
    var triggeredBy: String? = null
}
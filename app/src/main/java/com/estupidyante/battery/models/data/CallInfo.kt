package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class CallInfo : RealmObject() {
    // Incoming call time sum since boot
    var incomingCallTime = 0.0

    // Outgoing call time sum since boot
    var outgoingCallTime = 0.0

    // Non-call time sum since boot
    var nonCallTime = 0.0

    // Idle, offhook or ringing
    var callStatus: String? = null
}
package com.estupidyante.battery.models.ui

import com.github.mikephil.charting.data.Entry
import java.util.*

class ChartCard(var type: Int, var label: String, var color: Int) {
    var entries: List<Entry>
    var extras: DoubleArray?

    companion object {
        private const val TAG = "ChartCard"
    }

    init {
        entries = ArrayList()
        extras = null
    }
}
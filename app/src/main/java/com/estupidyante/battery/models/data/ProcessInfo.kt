package com.estupidyante.battery.models.data

import io.realm.RealmList
import io.realm.RealmObject

open class ProcessInfo : RealmObject() {
    // Process Id
    var processId = 0

    // Process Name
    var name: String? = null

    // Human readable application name
    var applicationLabel: String? = null

    // If the app is a system app or update to a system app
    var isSystemApp = false

    // Foreground, visible, background, service, empty
    var importance: String? = null

    // Version of app, human-readable
    var versionName: String? = null

    // Version of app, android version code
    var versionCode = 0

    // Package that installed this process, e.g. com.google.play
    var installationPkg: String? = null

    // Package Permissions
    var appPermissions: RealmList<AppPermission>? = null

    // Signatures of the app from PackageInfo.signatures (it can be empty)
    var appSignatures: RealmList<AppSignature>? = null
}
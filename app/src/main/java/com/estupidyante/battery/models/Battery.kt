package com.estupidyante.battery.models

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import com.estupidyante.battery.Config
import com.estupidyante.battery.managers.storage.BatteryAppDb
import com.estupidyante.battery.models.data.BatteryUsage
import com.estupidyante.battery.util.LogUtils.logI
import com.estupidyante.battery.util.LogUtils.makeLogTag
import io.realm.RealmResults
import java.io.*
import java.util.*
import kotlin.math.abs
import kotlin.math.min

object Battery {
    private val TAG: String = makeLogTag(Battery::class.java)

    private fun getBatteryVoltage(context: Context): Double {
        val receiver = context.registerReceiver(
            null,
            IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        )
            ?: return 0.0
        val voltage =
            receiver.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0).toDouble()
        return if (voltage == 0.0) voltage else voltage / 1000
    }


    private fun getBatteryCapacity(context: Context): Int {
        var value = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            value = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        }
        return if (value != 0 && value != Int.MIN_VALUE) {
            value
        } else 0
    }

    fun getBatteryDesignCapacity(context: Context?): Int {
        val mPowerProfile: Any
        var batteryCapacity = 0.0
        val POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile"
        try {
            mPowerProfile = Class.forName(POWER_PROFILE_CLASS)
                .getConstructor(Context::class.java)
                .newInstance(context)
            batteryCapacity = Class
                .forName(POWER_PROFILE_CLASS)
                .getMethod("getBatteryCapacity")
                .invoke(mPowerProfile) as Double
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return batteryCapacity.toInt()
    }

    fun getBatteryChargeCounter(context: Context): Int {
        var value = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            value = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER)
        }
        return value
    }

    fun getBatteryCurrentAverage(context: Context): Int {
        var value = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            value = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE)
        }
        return if (value != 0 && value != Int.MIN_VALUE) value else 0
    }

    fun getBatteryCurrentNow(context: Context): Int {
        var value = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            value = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW)
        }
        return if (value != 0 && value != Int.MIN_VALUE) value else 0
    }

    fun getBatteryCurrentNowInAmperes(context: Context): Double {
        var value = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            value = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW)
        }
        value = if (value != 0 && value != Int.MIN_VALUE) value else 0
        return value.toDouble() / 1000000
    }

    fun getBatteryEnergyCounter(context: Context): Long {
        var value: Long = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            value = manager.getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER)
        }

        return value // in mWh
    }

    fun getBatteryAveragePower(context: Context): Int {
        val voltage: Int
        var current = 0
        val receiver =
            context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
                ?: return -1
        voltage = receiver.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0)
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (manager != null) {
            current = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE)
        }
        return voltage * current / 1000000000
    }

    fun getBatteryCapacityConsumed(
        workload: Double,
        context: Context
    ): Double {
        var current = 0
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        current = manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE)
        return current * workload / 1000
    }

    fun getBatteryRemainingCapacity(context: Context): Int {
        val remainingCapacity: Double
        var capacity = getBatteryCapacity(context).toLong() // in %
        if (capacity <= -1) {
            capacity = 0
        }
        var chargeCounter = getBatteryChargeCounter(context).toLong()
        if (chargeCounter <= -1) {
            chargeCounter =
                Math.abs(getBatteryDesignCapacity(context)).toLong() // in mAh
        }
        if (capacity > 0 && chargeCounter > 0) {
            remainingCapacity = (chargeCounter * capacity / 100).toDouble()
        } else {
            val voltageNow =
                Math.max(1.0, getBatteryVoltage(context))
            var energyCounter = getBatteryEnergyCounter(context)
            if (energyCounter <= -1) {
                energyCounter = 0
            }
            return (energyCounter / voltageNow).toInt()
        }
        return remainingCapacity.toInt()
    }

    @Deprecated("")
    fun getRemainingBatteryTimeEstimate(context: Context?): Double {
        val remainingCapacity =
            getBatteryRemainingCapacity(context!!).toDouble()

        //in mA
        val currentNow =
            if (getBatteryCurrentNow(context) != -1) abs(
                getBatteryCurrentNow(context)
            ) else 0

        if (remainingCapacity > 0 && currentNow > 0) {
            return remainingCapacity.div(currentNow)
        }

        return (-1).toDouble()
    }

    fun getRemainingBatteryTime(
        context: Context, charging: Boolean,
        charger: String?
    ): Long {
        val remainingCapacity: Double
        val chargingSignal: Double
        var defaultDischargeRate: Int = Config.DEFAULT_DISCHARGE_RATE
        if (!charging) {
            chargingSignal = -1.0
            remainingCapacity = getBatteryRemainingCapacity(context).toDouble()
            logI("WOW", "[B] RemCap: $remainingCapacity")
        } else {
            chargingSignal = 1.0
            when (charger) {
                "usb" -> defaultDischargeRate = Config.DEFAULT_USB_CHARGE_RATE
                "ac" -> defaultDischargeRate = Config.DEFAULT_AC_CHARGE_RATE
                "wireless" -> defaultDischargeRate = Config.DEFAULT_WIRELESS_CHARGE_RATE
            }
            val fullCapacity =
                if (getBatteryChargeCounter(context) != -1) getBatteryChargeCounter(
                    context
                ) else getBatteryDesignCapacity(context)
            remainingCapacity =
                fullCapacity - getBatteryRemainingCapacity(context).toDouble()
        }
        val database = BatteryAppDb()
        val allUsages: RealmResults<BatteryUsage> = database.usages
        val limit = min(Config.BATTERY_CAPACITY_SAMPLES_SIZE, allUsages.size)
        if (limit <= 1) {
            // no samples collected yet
            // consider a naive value
            logI(
                TAG, "Not enough samples yet in the DB." +
                        "Assuming a blind estimation of battery remaining time."
            )
            return (remainingCapacity * (60 * 60) / defaultDischargeRate).toLong()
        }

        logI(TAG, "Estimating battery remaining time using $limit samples")
        val lastUsages: ArrayList<BatteryUsage?> = ArrayList<BatteryUsage?>(allUsages.subList(0, limit))
        val dischargeSamples =
            ArrayList<Double>()
        var previousUsage: BatteryUsage? = null
        for (currentUsage in lastUsages) {
            if (previousUsage == null) {
                previousUsage = currentUsage
                continue
            }
            val currentCapacity: Int = currentUsage?.details!!.remainingCapacity
            val previousCapacity: Int = previousUsage.details!!.remainingCapacity
            val discharge = chargingSignal * (previousCapacity - currentCapacity)

            // prevent division by zero OR a negative charge/discharge:
            // i.e., only positive differences are considered
            if (discharge <= 0.0) {
                previousUsage = currentUsage
                continue
            }
            // in seconds
            val elapsedTime: Long = abs(
                (currentUsage.timestamp - previousUsage.timestamp) / 1000
            )
            dischargeSamples.add(elapsedTime / discharge)
            previousUsage = currentUsage
        }
        database.close()
        if (dischargeSamples.size == 0) {
            return (remainingCapacity * (60 * 60) / defaultDischargeRate).toLong()
        }
        var sumDischarges = 0.0
        for (discharge in dischargeSamples) {
            sumDischarges += discharge
        }
        val dischargeRatio = sumDischarges / dischargeSamples.size
        return (remainingCapacity * dischargeRatio).toLong()
    }

    // Device has no current_avg file available
    private val batteryCurrentNowLegacy: Int
        get() {
            var value = 0
            try {
                val reader =
                    RandomAccessFile(Config.BATTERY_SOURCE_DEFAULT, "r")
                val average: String = reader.readLine()
                value = average.toInt()
                reader.close()
            } catch (e: IOException) {
                // Device has no current_avg file available
                logI(TAG, "Device has no current_avg file available")
            }
            return value
        }

    private fun getBatteryPropertyLegacy(property: String): Int {
        var value = 0
        val reader: BufferedReader
        val file = File(Config.BATTERY_STATS_SOURCE_DEFAULT)
        if (file.exists()) {
            try {
                reader =
                    BufferedReader(InputStreamReader(FileInputStream(file)))
                var line = reader.readLine()
//                while (line != null && value == 0) {
//                    if (line.matches("$property.*")) {
//                        val splittedLine =
//                            line.split("=").toTypedArray()
//                        if (splittedLine.size == 2) {
//                            value = splittedLine[1].toInt()
//                        }
//                    }
//                    line = reader.readLine()
//                }
            } catch (e: IOException) {
                logI(TAG, "Could not read from standard battery stats file")
            }
        } else {
            logI(
                TAG,
                "Standard battery stats file does not exist or is not accessible"
            )
        }
        return value
    }

    fun logAllBatteryValues(context: Context) {
        val manager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        logI("Battery Voltage", "v: " + getBatteryVoltage(context))
        logI(
            "[API] Battery Capacity", "v: " +
                    manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        )
        logI(
            "[API] Battery Charge Counter", "v: " +
                    manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER)
        )
        logI(
            "[API] Battery Energy Counter", "v: " +
                    manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER)
        )
        logI(
            "[API] Battery Current Average", "v: " +
                    manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE)
        )
        logI(
            "[API] Battery Current Now", "v: " +
                    manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW)
        )
        logI(
            "Battery Capacity", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_CAPACITY)
        )
        logI(
            "Battery Charge Counter", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_CHARGE_FULL)
        )
        logI(
            "Battery Charge Counter (Design)", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_CHARGE_FULL_DESIGN)
        )
        logI(
            "Battery Energy Counter", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_ENERGY_FULL)
        )
        logI(
            "Battery Energy Counter (Design)", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_ENERGY_FULL_DESIGN)
        )
        logI(
            "Battery Current Now", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_CURRENT_NOW)
        )
        logI(
            "Battery Current Now (2)", "v: " +
                    batteryCurrentNowLegacy
        )
        logI(
            "Battery Energy Now", "v: " +
                    getBatteryPropertyLegacy(Config.BATTERY_ENERGY_NOW)
        )

        // Reflections
        logI("Actual Battery Capacity", "v: " + getBatteryDesignCapacity(context))
    }
}


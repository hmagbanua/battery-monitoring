package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class CpuStatus : RealmObject() {
    // CPU usage fraction (0-1)
    var cpuUsage = 0.0

    // Uptime in seconds
    var upTime: Long = 0

    // Experimental sleep time
    var sleepTime: Long = 0
}
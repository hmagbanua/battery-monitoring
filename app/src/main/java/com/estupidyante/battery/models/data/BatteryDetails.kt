package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class BatteryDetails : RealmObject() {
    // Currently ac, usb, or unplugged
    var charger: String? = null

    // Currently Unknown, Unspecified failure, Dead, Cold, Overheat, Over voltage or Good
    var health: String? = null

    // Voltage in Volts
    var voltage = 0.0

    // Temperature in Celsius
    var temperature = 0.0

    // Battery technology
    var technology: String? = null

    // Total capacity in mAh
    var capacity = 0

    // Remaining capacity in mAh
    var remainingCapacity = 0

    // Battery capacity in microAmpere-hours
    var chargeCounter = 0

    // Average battery current in microAmperes
    var currentAverage = 0

    // Instantaneous battery current in milliAmperes
    var currentNow = 0

    // Battery remaining energy in nanoWatt-hours
    var energyCounter: Long = 0 // age factor ...
}
package com.estupidyante.battery.models

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.preference.PreferenceManager
import com.estupidyante.battery.Config
import com.estupidyante.battery.models.data.Feature
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.util.*

object Specifications {
    private val TAG: String = makeLogTag(Specifications::class.java)

    private fun getSharedPreferences(context: Context): SharedPreferences? {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    private fun setAndroidId(
        context: Context,
        id: String
    ) {
        val sp: SharedPreferences? = getSharedPreferences(context)
        sp!!.edit().putString(PREF_UNIQUE_ID, id).apply()
        LogUtils.logD(TAG, "Android ID Key of device set to: $id")
    }

    private fun checkRootMethod1(): Boolean {
        val buildTags = Build.TAGS
        return buildTags != null && buildTags.contains("test-keys")
    }

    private fun checkRootMethod2(): Boolean {
        val paths = arrayOf(
            "/system/app/Superuser.apk",
            "/sbin/su",
            "/system/bin/su",
            "/system/xbin/su",
            "/data/local/xbin/su",
            "/data/local/bin/su",
            "/system/sd/xbin/su",
            "/system/bin/failsafe/su",
            "/data/local/su"
        )
        for (path in paths) {
            if (File(path).exists()) return true
        }
        return false
    }

    private fun checkRootMethod3(): Boolean {
        var process: Process? = null
        return try {
            process = Runtime.getRuntime()
                .exec(arrayOf("/system/xbin/which", "su"))
            val `in` =
                BufferedReader(InputStreamReader(process.inputStream))
            `in`.readLine() != null
        } catch (t: Throwable) {
            false
        } finally {
            process?.destroy()
        }
    }

    private const val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"
    private const val TYPE_UNKNOWN = "unknown"
    private const val UUID_LENGTH = 16

    @Synchronized
    fun getAndroidId(specifications: Specifications, context: Context?): String? {
        val sp: SharedPreferences? = specifications.getSharedPreferences(context!!)
        var id = sp?.getString(PREF_UNIQUE_ID, null)
        if (id == null) {
            id = UUID.randomUUID().toString()
            LogUtils.logD(
                specifications.TAG,
                "No Android ID Key on this device. Generating random one: $id"
            )
            specifications.setAndroidId(context, id)
        }
        return id
    }

    fun getBrand(): String? {
        return Build.BRAND
    }

    fun getBuildSerial(): String? {
        return Build.SERIAL
    }

    fun getKernelVersion(): String? {
        return System.getProperty("os.version", TYPE_UNKNOWN)
    }

    fun getManufacturer(): String? {
        return Build.MANUFACTURER
    }

    fun getModel(): String? {
        return Build.MODEL
    }

    fun getOsVersion(): String? {
        return Build.VERSION.RELEASE
    }

    fun getProductName(): String? {
        return Build.PRODUCT
    }

    fun getVmVersion(): Feature {
        val vmVersion = Feature()
        var vm = System.getProperty("java.vm.version")
        if (vm == null) vm = ""
        vmVersion.key = "vm"
        vmVersion.value = vm
        return vmVersion
    }

    @Throws(java.lang.Exception::class)
    fun getSystemProperty(
        context: Context?,
        property: String?
    ): String? {
        val systemProperties =
            Class.forName("android.os.SystemProperties")
        val get =
            systemProperties.getMethod("get", String::class.java)
        get.isAccessible = true
        return get.invoke(context, property) as String
    }

    fun getStringFromSystemProperty(
        context: Context?,
        property: String?
    ): String? {
        try {
            val operator: String? = getSystemProperty(context, property)
            if (operator != null && operator.isNotEmpty()) {
                return operator
            }
        } catch (e: Exception) {
            if (Config.DEBUG && e.localizedMessage != null) {
                LogUtils.logD(
                    TAG,
                    "Failed getting service provider: " + e.localizedMessage
                )
            }
        }
        return null
    }

    fun isRooted(): Boolean {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3()
    }
}
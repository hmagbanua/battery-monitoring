package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class Settings : RealmObject() {
    var bluetoothEnabled = false
    var locationEnabled = false
    var powersaverEnabled = false
    var flashlightEnabled = false
    var nfcEnabled = false

    // Unknown source app installation on == 1, off == 0
    var unknownSources = 0

    // Developer mode on == 1, off == 0
    var developerMode = 0
}
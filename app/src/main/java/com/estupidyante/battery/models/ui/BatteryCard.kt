package com.estupidyante.battery.models.ui

import android.graphics.Color
import androidx.annotation.DrawableRes

class BatteryCard {
    var icon: Int
    var label: String
    var value: String
    var indicator: Int

    constructor(@DrawableRes icon: Int, title: String, value: String) {
        this.icon = icon
        label = title
        this.value = value
        indicator = Color.GREEN
    }

    constructor(
        @DrawableRes icon: Int,
        title: String,
        value: String,
        indicator: Int
    ) {
        this.icon = icon
        label = title
        this.value = value
        this.indicator = indicator
    }
}
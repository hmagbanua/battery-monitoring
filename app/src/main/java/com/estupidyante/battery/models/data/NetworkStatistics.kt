package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class NetworkStatistics : RealmObject() {
    // Amount of wifi data received
    var wifiReceived = 0.0

    // Amount of wifi data sent
    var wifiSent = 0.0

    // Amount of mobile data received
    var mobileReceived = 0.0

    // Amount of mobile data sent
    var mobileSent = 0.0
}
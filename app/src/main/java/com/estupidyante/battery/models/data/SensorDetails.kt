package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class SensorDetails : RealmObject() {
    var fifoMaxEventCount = 0
    var fifoReservedEventCount = 0

    //Get the highest supported direct report mode rate level of the sensor.
    var highestDirectReportRateLevel = 0
    var id = 0

    //This value is defined only for continuous and on-change sensors.
    var maxDelay = 0
    var maximumRange = 0f
    var minDelay = 0
    var name: String? = null

    //the power in mA used by this sensor while in use
    var power = 0f

    //Each sensor has exactly one reporting mode associated with it.
    var reportingMode = 0
    var resolution = 0f
    var stringType: String? = null
    var codeType = 0
    var vendor: String? = null
    var version = 0

    //Returns true if the sensor supports sensor additional information API
    var isAdditionalInfoSupported = false

    //Returns true if the sensor is a dynamic sensor.
    var isDynamicSensor = false

    //https://developer.android.com/reference/android/hardware/Sensor.html#isWakeUpSensor()
    var isWakeUpSensor = false

    //Number of sensor events in a given time interval
    var frequencyOfUse = 0

    //start sample usage Count
    var iniTimestamp: Long = 0

    //end of sample usage count
    var endTimestamp: Long = 0
}
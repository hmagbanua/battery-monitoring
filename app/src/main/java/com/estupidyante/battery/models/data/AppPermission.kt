package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class AppPermission : RealmObject {
    var permission: String? = null

    constructor() {}
    constructor(permission: String?) {
        this.permission = permission
    }
}
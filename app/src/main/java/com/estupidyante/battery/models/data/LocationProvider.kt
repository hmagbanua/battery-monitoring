package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class LocationProvider : RealmObject {
    var provider: String? = null

    constructor() {}
    constructor(provider: String?) {
        this.provider = provider
    }
}
package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class StorageDetails : RealmObject() {
    var free = 0
    var total = 0
    var freeExternal = 0
    var totalExternal = 0
    var freeSystem = 0
    var totalSystem = 0
    var freeSecondary = 0
    var totalSecondary = 0
}
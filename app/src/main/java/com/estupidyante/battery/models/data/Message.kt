package com.estupidyante.battery.models.data

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Message : RealmObject {
    @PrimaryKey
    var id = 0
    var type: String
    var title: String? = null
    var body: String? = null
    var date: String? = null
    var read: Boolean

    constructor() {
        type = "info"
        read = false
    }

    constructor(id: Int, title: String?, body: String?, date: String?) {
        this.id = id
        type = "info"
        this.title = title
        this.body = body
        this.date = date
        read = false
    }

    constructor(
        id: Int,
        title: String?,
        body: String?,
        date: String?,
        read: Boolean
    ) {
        this.id = id
        type = "info"
        this.title = title
        this.body = body
        this.date = date
        this.read = read
    }
}
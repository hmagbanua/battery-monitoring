package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class Feature : RealmObject() {
    var key: String? = null
    var value: String? = null
}
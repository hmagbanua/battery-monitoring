package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class AppSignature : RealmObject {
    var signature: String? = null

    constructor() {}
    constructor(signature: String?) {
        this.signature = signature
    }
}
package com.estupidyante.battery.models.data

import io.realm.RealmObject

open class NetworkDetails : RealmObject() {
    // wifi, mobile or unknown
    var networkType: String? = null

    // GPRS, EDGE, UMTS, etc.
    var mobileNetworkType: String? = null

    // connecting, connected, disconnected, suspended
    var mobileDataStatus: String? = null

    // none, in, out, inout, dormant
    var mobileDataActivity: String? = null

    // 1 if currently roaming in a foreign mobile network, 0 otherwise
    var roamingEnabled = 0

    // disabled, disabling, enabled, enabling, unknown
    var wifiStatus: String? = null

    // As given by getRssi()
    var wifiSignalStrength = 0

    // Link speed in Mbps
    var wifiLinkSpeed = 0

    // Sent and received data
    var networkStatistics: NetworkStatistics? = null

    // Wifi access point status: disabled, disabling, enabled, enabling, unknown
    var wifiApStatus: String? = null

    // Network infrastructure provider, unbound
    var networkOperator: String? = null

    // Service provider, bound to sim
    var simOperator: String? = null

    // Numeric country code
    var mcc: String? = null

    // Numeric network code
    var mnc: String? = null
}
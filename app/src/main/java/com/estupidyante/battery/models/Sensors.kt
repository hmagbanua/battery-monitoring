package com.estupidyante.battery.models

import android.annotation.TargetApi
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager
import android.os.Build
import com.estupidyante.battery.models.data.SensorDetails
import com.estupidyante.battery.util.LogUtils.makeLogTag
import java.util.*

object Sensors {
    private val TAG: String = makeLogTag(Sensors::class.java)
    private val SDK_VERSION = Build.VERSION.SDK_INT
    private var sensorsMap: MutableMap<String, SensorDetails?> =
        HashMap<String, SensorDetails?>()
    private var manager: SensorManager? = null

    /**
     * Obtains the current Fifo Max Event Count.
     *
     * @param context Application's context
     * @return Returns the battery voltage
     */
    fun getSensorDetailsList(context: Context): Collection<SensorDetails?> {
        verifySensorsChanged(context)
        return sensorsMap.values
    }

    private fun verifySensorsChanged(context: Context) {
        if (manager == null) {
            manager =
                context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        }
        assert(manager != null)
        val values =
            manager!!.getSensorList(Sensor.TYPE_ALL)
        if (values.size != sensorsMap.size) {
            sensorsMap.clear()
            for (sensor in values) {
                extractSensorDetails(sensor)
            }
        }
    }

    private fun extractSensorDetails(sensor: Sensor): SensorDetails {
        var details: SensorDetails? = null
        if (sensorsMap[sensor.name].also({ details = it }) == null) {
            details = SensorDetails()
            sensorsMap[sensor.name] = details
        }
        details?.codeType = sensor.type
        details?.fifoMaxEventCount = sensor.fifoMaxEventCount
        details?.fifoReservedEventCount = sensor.fifoReservedEventCount
        getAttributesNewVersion(sensor, details!!)
        details?.isWakeUpSensor = sensor.isWakeUpSensor
        details?.maxDelay = sensor.maxDelay
        details?.maximumRange = sensor.maximumRange
        details?.minDelay = sensor.minDelay
        details?.name = sensor.name
        details?.power = sensor.power
        details?.reportingMode = sensor.reportingMode
        details?.resolution = sensor.resolution
        details?.stringType = sensor.stringType
        details?.vendor = sensor.vendor
        details?.version = sensor.version
        return details!!
    }

    /**
     * Update events of the sensors
     * @param event SensorEvent
     */
    fun onSensorChanged(event: SensorEvent) {
        val details: SensorDetails = extractSensorDetails(event.sensor)
        details.frequencyOfUse++
        if (details.iniTimestamp === 0L) {
            details.iniTimestamp = event.timestamp
        }
        details.endTimestamp = event.timestamp
    }

    fun clearSensorsMap() {
        sensorsMap = HashMap<String, SensorDetails?>()
    }

    @TargetApi(23)
    private fun getAttributesNewVersion(
        sensor: Sensor,
        details: SensorDetails
    ) {
        if (SDK_VERSION > 23) {
            details.id = sensor.id
            details.isAdditionalInfoSupported = sensor.isAdditionalInfoSupported
            details.isDynamicSensor = sensor.isDynamicSensor
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            details.highestDirectReportRateLevel = sensor.highestDirectReportRateLevel
        }
    }
}
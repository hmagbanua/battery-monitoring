package com.estupidyante.battery.models.data

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class Sample : RealmObject() {
    @PrimaryKey
    var id = 0

    // ID for the current device
    var uuId: String? = null

    // Timestamp of sample created time
    @Index
    var timestamp: Long = 0

    // Mobile app version of sample
    var version = 0

    // Mobile mDatabase version of sample
    var database = 0

    // State of the battery. ie. charging, discharging, etc.
    var batteryState: String? = null

    // Level of the battery (0 - 1.0) translates to percentage
    var batteryLevel = 0.0

    // Total wired memory
    var memoryWired = 0

    // Total active memory
    var memoryActive = 0

    // Total inactive memory
    var memoryInactive = 0

    // Total free memory
    var memoryFree = 0

    // Total user memory
    var memoryUser = 0

    // Trigger reason
    var triggeredBy: String? = null

    // Reachability status
    var networkStatus: String? = null

    // If locationchange triggers, then this will have a value
    var distanceTraveled = 0.0

    // Brightness value, 0-255
    var screenBrightness = 0

    // Network status struct, with info on the active network, mobile,  and wifi
    var networkDetails: NetworkDetails? = null

    // Battery status struct, with battery health, charger, voltage, temperature, etc.
    var batteryDetails: BatteryDetails? = null

    // CPU information, such as cpu usage percentage
    var cpuStatus: CpuStatus? = null

    // Call ratios and information
    var callInfo: CallInfo? = null

    // If screen is on == 1, off == 0
    var screenOn = 0

    // Device timezone abbreviation
    var timeZone: String? = null

    // Current set of SettingsInfo
    var settings: Settings? = null

    // Current storage details of device
    var storageDetails: StorageDetails? = null

    // Two-letter country code from network or SIM
    var countryCode: String? = null

    // Enabled location providers
    var locationProviders: RealmList<LocationProvider>? = null

    // List of processes running
    var processInfos: RealmList<ProcessInfo>? = null

    // Extra features for extensibility
    var features: RealmList<Feature>? = null

    // Sensor list struct, with sensor power, type, isWakeUp, etc.
    var sensorDetailsList: RealmList<SensorDetails>? = null
}
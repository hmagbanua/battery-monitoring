package com.estupidyante.battery

object Config {

    const val DEBUG = true
    const val SERVER_URL_DEFAULT = "none"
    const val SERVER_URL_DEVELOPMENT = "http://192.168.1.95:8080"
    const val DATABASE_VERSION = 5
    const val IMPORTANCE_NOT_RUNNING = "Not Running"
    const val IMPORTANCE_UNINSTALLED = "uninstalled"
    const val IMPORTANCE_DISABLED = "disabled"
    const val IMPORTANCE_INSTALLED = "installed"
    const val IMPORTANCE_REPLACED = "replaced"
    const val IMPORTANCE_APP = 9999
    const val BATTERY_SOURCE_DEFAULT = "/sys/class/power_supply/battery/current_now"
    const val BATTERY_SOURCE_ALTERNATIVE =
        "/sys/devices/platform/battery/power_supply/battery/BatteryAverageCurrent"
    const val BATTERY_STATS_SOURCE_DEFAULT = "/sys/class/power_supply/battery/uevent"

    // Battery Full Capacity in uAh
    const val BATTERY_CHARGE_FULL = "POWER_SUPPLY_CHARGE_FULL"
    const val BATTERY_CHARGE_FULL_DESIGN = "POWER_SUPPLY_CHARGE_FULL_DESIGN"

    // Battery Full Capacity in uWh
    const val BATTERY_ENERGY_FULL = "POWER_SUPPLY_ENERGY_FULL"
    const val BATTERY_ENERGY_FULL_DESIGN = "POWER_SUPPLY_CHARGE_FULL_DESIGN"

    // The circuit voltage at the moment
    const val BATTERY_VOLTAGE_NOW = "POWER_SUPPLY_VOLTAGE_NOW"

    // The Current at the moment in uA
    const val BATTERY_CURRENT_NOW = "POWER_SUPPLY_CURRENT_NOW"

    // The device remaining capacity in uAh
    const val BATTERY_ENERGY_NOW = "POWER_SUPPLY_ENERGY_NOW"

    // The battery capacity percentage (1-100)
    const val BATTERY_CAPACITY = "POWER_SUPPLY_CAPACITY"
    const val BATTERY_CAPACITY_SAMPLES_SIZE = 10

    // assume that, in one hours, it charges 500 mA
    const val DEFAULT_USB_CHARGE_RATE = 500

    // assume that, in one hours, it charges 1500 mA
    const val DEFAULT_AC_CHARGE_RATE = 1500

    // assume that, in one hours, it charges 500 mA
    const val DEFAULT_WIRELESS_CHARGE_RATE = 500

    // assume a continuous discharge of 200 mA per hour
    const val DEFAULT_DISCHARGE_RATE = 200
    const val DATA_HISTORY_DEFAULT = "4"
    const val UPLOAD_MAX_TRIES = 3
    const val UPLOAD_DEFAULT_RATE = "20"
    const val SAMPLES_MAX_STORAGE_NUM = 500
    const val STARTER_MESSAGE_ID = 0
    const val STARTUP_CURRENT_INTERVAL = 2000
    const val REFRESH_CURRENT_INTERVAL = 5000L
    const val REFRESH_MEMORY_INTERVAL = 10000
    const val REFRESH_STATUS_BAR_INTERVAL = REFRESH_CURRENT_INTERVAL * 6
    const val REFRESH_STATUS_ERROR = REFRESH_CURRENT_INTERVAL * 2
    const val NOTIFICATION_DEFAULT_TEMPERATURE_RATE = "5"
    const val NOTIFICATION_DEFAULT_TEMPERATURE_WARNING = "35"
    const val NOTIFICATION_DEFAULT_TEMPERATURE_HIGH = "45"
    const val BATTERY_LOW_LEVEL = 0.2
    const val PERMISSION_READ_PHONE_STATE = 1
    const val PERMISSION_ACCESS_COARSE_LOCATION = 2
    const val PERMISSION_ACCESS_FINE_LOCATION = 3
    const val NOTIFICATION_DEFAULT_PRIORITY = "0"
    const val NOTIFICATION_BATTERY_STATUS = 1001
    const val NOTIFICATION_BATTERY_FULL = 1002
    const val NOTIFICATION_BATTERY_LOW = 1003
    const val NOTIFICATION_TEMPERATURE_WARNING = 1004
    const val NOTIFICATION_TEMPERATURE_HIGH = 1005
    const val NOTIFICATION_MESSAGE_NEW = 1006
    const val PENDING_REMOVAL_TIMEOUT = 1500 // 1.5s
    const val KILL_APP_TIMEOUT = 15000 // 15s
    const val SORT_BY_MEMORY = 1
    const val SORT_BY_NAME = 2
    val PERMISSIONS_ARRAY = arrayOf(
        "android.permission.ACCESS_CHECKIN_PROPERTIES",
        "android.permission.ACCESS_COARSE_LOCATION",
        "android.permission.ACCESS_FINE_LOCATION",
        "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS",
        "android.permission.ACCESS_MOCK_LOCATION",
        "android.permission.ACCESS_NETWORK_STATE",
        "android.permission.ACCESS_SURFACE_FLINGER",
        "android.permission.ACCESS_WIFI_STATE",
        "android.permission.ACCOUNT_MANAGER",
        "android.permission.AUTHENTICATE_ACCOUNTS",
        "android.permission.BATTERY_STATS",
        "android.permission.BIND_APPWIDGET",
        "android.permission.BIND_DEVICE_ADMIN",
        "android.permission.BIND_INPUT_METHOD",
        "android.permission.BIND_WALLPAPER",
        "android.permission.BLUETOOTH",
        "android.permission.BLUETOOTH_ADMIN",
        "android.permission.BRICK",
        "android.permission.BROADCAST_PACKAGE_REMOVED",
        "android.permission.BROADCAST_SMS",
        "android.permission.BROADCAST_STICKY",
        "android.permission.BROADCAST_WAP_PUSH",
        "android.permission.CALL_PHONE",
        "android.permission.CALL_PRIVILEGED",
        "android.permission.CAMERA",
        "android.permission.CHANGE_COMPONENT_ENABLED_STATE",
        "android.permission.CHANGE_CONFIGURATION",
        "android.permission.CHANGE_NETWORK_STATE",
        "android.permission.CHANGE_WIFI_MULTICAST_STATE",
        "android.permission.CHANGE_WIFI_STATE",
        "android.permission.CLEAR_APP_CACHE",
        "android.permission.CLEAR_APP_USER_DATA",
        "android.permission.CONTROL_LOCATION_UPDATES",
        "android.permission.DELETE_CACHE_FILES",
        "android.permission.DELETE_PACKAGES",
        "android.permission.DEVICE_POWER",
        "android.permission.DIAGNOSTIC",
        "android.permission.DISABLE_KEYGUARD",
        "android.permission.DUMP",
        "android.permission.EXPAND_STATUS_BAR",
        "android.permission.FACTORY_TEST",
        "android.permission.FLASHLIGHT",
        "android.permission.FORCE_BACK",
        "android.permission.GET_ACCOUNTS",
        "android.permission.GET_PACKAGE_SIZE",
        "android.permission.GET_TASKS",
        "android.permission.GLOBAL_SEARCH",
        "android.permission.HARDWARE_TEST",
        "android.permission.INJECT_EVENTS",
        "android.permission.INSTALL_LOCATION_PROVIDER",
        "android.permission.INSTALL_PACKAGES",
        "android.permission.INTERNAL_SYSTEM_WINDOW",
        "android.permission.INTERNET",
        "android.permission.KILL_BACKGROUND_PROCESSES",
        "android.permission.MANAGE_ACCOUNTS",
        "android.permission.MANAGE_APP_TOKENS",
        "android.permission.MASTER_CLEAR",
        "android.permission.MODIFY_AUDIO_SETTINGS",
        "android.permission.MODIFY_PHONE_STATE",
        "android.permission.MOUNT_FORMAT_FILESYSTEMS",
        "android.permission.MOUNT_UNMOUNT_FILESYSTEMS",
        "android.permission.PERSISTENT_ACTIVITY",
        "android.permission.PROCESS_OUTGOING_CALLS",
        "android.permission.READ_CALENDAR",
        "android.permission.READ_CONTACTS",
        "android.permission.READ_FRAME_BUFFER",
        "com.android.browser.permission.READ_HISTORY_BOOKMARKS",
        "android.permission.READ_INPUT_STATE",
        "android.permission.READ_LOGS",
        "android.permission.READ_OWNER_DATA",
        "android.permission.READ_PHONE_STATE",
        "android.permission.READ_SMS",
        "android.permission.READ_SYNC_SETTINGS",
        "android.permission.READ_SYNC_STATS",
        "android.permission.REBOOT",
        "android.permission.RECEIVE_BOOT_COMPLETED",
        "android.permission.RECEIVE_MMS",
        "android.permission.RECEIVE_SMS",
        "android.permission.RECEIVE_WAP_PUSH",
        "android.permission.RECORD_AUDIO",
        "android.permission.REORDER_TASKS",
        "android.permission.RESTART_PACKAGES",
        "android.permission.SEND_SMS",
        "android.permission.SET_ACTIVITY_WATCHER",
        "android.permission.SET_ALWAYS_FINISH",
        "android.permission.SET_ANIMATION_SCALE",
        "android.permission.SET_DEBUG_APP",
        "android.permission.SET_ORIENTATION",
        "android.permission.SET_PREFERRED_APPLICATIONS",
        "android.permission.SET_PROCESS_LIMIT",
        "android.permission.SET_TIME",
        "android.permission.SET_TIME_ZONE",
        "android.permission.SET_WALLPAPER",
        "android.permission.SET_WALLPAPER_HINTS",
        "android.permission.SIGNAL_PERSISTENT_PROCESSES",
        "android.permission.STATUS_BAR",
        "android.permission.SUBSCRIBED_FEEDS_READ",
        "android.permission.SUBSCRIBED_FEEDS_WRITE",
        "android.permission.SYSTEM_ALERT_WINDOW",
        "android.permission.UPDATE_DEVICE_STATS",
        "android.permission.USE_CREDENTIALS",
        "android.permission.VIBRATE",
        "android.permission.WAKE_LOCK",
        "android.permission.WRITE_APN_SETTINGS",
        "android.permission.WRITE_CALENDAR",
        "android.permission.WRITE_CONTACTS",
        "android.permission.WRITE_EXTERNAL_STORAGE",
        "android.permission.WRITE_GSERVICES",
        "com.android.browser.permission.WRITE_HISTORY_BOOKMARKS",
        "android.permission.WRITE_OWNER_DATA",
        "android.permission.WRITE_SECURE_SETTINGS",
        "android.permission.WRITE_SETTINGS",
        "android.permission.WRITE_SMS",
        "android.permission.WRITE_SYNC_SETTINGS"
    )
}
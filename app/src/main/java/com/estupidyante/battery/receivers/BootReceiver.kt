package com.estupidyante.battery.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag

class BootReceiver : BroadcastReceiver() {
    /**
     * Used to start Service on reboot even when GreenHub is not started.
     *
     * @param context the context
     * @param intent the intent (should be ACTION_BOOT_COMPLETED)
     */
    override fun onReceive(context: Context, intent: Intent) {
        LogUtils.logI(TAG, "BOOT_COMPLETED onReceive()")
    }

    companion object {
        private val TAG: String = makeLogTag(BootReceiver::class.java)
    }
}
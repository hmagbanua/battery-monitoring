package com.estupidyante.battery.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.ReceiverCallNotAllowedException
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.Notifier

class NotificationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        LogUtils.logI(TAG, "onReceive called!")
        try {
            Notifier.updateStatusBar(context)
        } catch (e: ReceiverCallNotAllowedException) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG: String = LogUtils.makeLogTag(NotificationReceiver::class.java)
    }
}
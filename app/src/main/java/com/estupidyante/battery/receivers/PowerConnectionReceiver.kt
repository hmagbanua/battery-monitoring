package com.estupidyante.battery.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import com.estupidyante.battery.events.BatteryTimeEvent
import com.estupidyante.battery.events.PowerSourceEvent
import com.estupidyante.battery.managers.sampling.Inspector
import com.estupidyante.battery.managers.storage.BatteryAppDb
import com.estupidyante.battery.models.Battery
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag
import io.realm.exceptions.RealmMigrationNeededException
import org.greenrobot.eventbus.EventBus

class PowerConnectionReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action ?: return
        var isCharging = false
        var batteryCharger = ""
        if (intent.action == Intent.ACTION_POWER_CONNECTED) {
            isCharging = true
            val mIntent = context.applicationContext
                .registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
                ?: return
            val chargePlug = mIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
            val usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB
            val acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC
            var wirelessCharge = false
            if (Build.VERSION.SDK_INT >= 24) {
                wirelessCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS
            }
            when {
                acCharge -> {
                    batteryCharger = "ac"
                    EventBus.getDefault().post(PowerSourceEvent("ac"))
                }
                usbCharge -> {
                    batteryCharger = "usb"
                    EventBus.getDefault().post(PowerSourceEvent("usb"))
                }
                wirelessCharge -> {
                    batteryCharger = "wireless"
                    EventBus.getDefault().post(PowerSourceEvent("wireless"))
                }
            }
        } else if (intent.action == Intent.ACTION_POWER_DISCONNECTED) {
            isCharging = false
            EventBus.getDefault().post(PowerSourceEvent("unplugged"))
        }
        // Post to subscribers & update notification
        val batteryRemaining =
            (Battery.getRemainingBatteryTime(context, isCharging, batteryCharger) / 60).toInt()
        val batteryRemainingHours = batteryRemaining / 60
        val batteryRemainingMinutes = batteryRemaining % 60
        EventBus.getDefault().post(
            BatteryTimeEvent(batteryRemainingHours, batteryRemainingMinutes, isCharging)
        )

        try {
            // Save a new Battery Session to the mDatabase
            val database = BatteryAppDb()
            LogUtils.logI(TAG, "Getting new session")
            database.saveSession(Inspector.getBatterySession(context, intent))
            database.close()
        } catch (e: IllegalStateException) {
            LogUtils.logE(TAG, "No session was created")
            e.printStackTrace()
        } catch (e: RealmMigrationNeededException) {
            LogUtils.logE(TAG, "No session was created")
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG: String = makeLogTag(PowerConnectionReceiver::class.java)
    }
}
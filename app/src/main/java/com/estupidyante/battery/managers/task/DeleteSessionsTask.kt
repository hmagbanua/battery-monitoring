package com.estupidyante.battery.managers.task

import android.os.AsyncTask
import com.estupidyante.battery.models.data.BatterySession
import com.estupidyante.battery.util.DateUtils
import io.realm.Realm
import io.realm.RealmResults

class DeleteSessionsTask :
    AsyncTask<Int?, Void?, Boolean>() {
    private var mResponse = false

    override fun doInBackground(vararg params: Int?): Boolean {
        mResponse = false
        // Open the Realm
        // Open the Realm
        val realm = Realm.getDefaultInstance()

        try {
            val interval = params[0]!!
            realm.executeTransaction { realm ->
                val sessions: RealmResults<BatterySession> = realm.where(
                    BatterySession::class.java
                ).lessThan(
                    "timestamp",
                    DateUtils.getMilliSecondsInterval(interval)
                ).findAll()
                if (sessions != null && !sessions.isEmpty()) {
                    mResponse = sessions.deleteAllFromRealm()
                }
            }
        } finally {
            realm.close()
        }

        return mResponse
    }
}
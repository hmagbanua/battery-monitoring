package com.estupidyante.battery.managers.storage

import androidx.annotation.NonNull
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.makeLogTag
import io.realm.DynamicRealm
import io.realm.RealmMigration
import io.realm.RealmObjectSchema

class BatteryAppDbMigration : RealmMigration {
    override fun migrate(
        @NonNull realm: DynamicRealm,
        oldVersion: Long,
        newVersion: Long
    ) {
        var oldVersion = oldVersion
        var objectSchema: RealmObjectSchema?

        // DynamicRealm exposes an editable schema
        val schema = realm.schema ?: return
        try {
            if (oldVersion == 1L) {
                objectSchema = schema["Sample"]
                if (objectSchema != null) {
                    objectSchema.addField("version", Int::class.javaPrimitiveType!!)
                    oldVersion++
                }
            }
            if (oldVersion == 2L) {
                var migrated = true
                objectSchema = schema["Device"]
                when {
                    objectSchema != null -> {
                        objectSchema.removeField("serialNumber")
                    }
                    else -> {
                        migrated = false
                    }
                }
                objectSchema = schema["Sample"]
                when {
                    objectSchema != null -> {
                        objectSchema.addField("mDatabase", Int::class.javaPrimitiveType!!)
                    }
                    else -> {
                        migrated = false
                    }
                }
                if (migrated) {
                    oldVersion++
                }
            }
            if (oldVersion == 3L) {
                objectSchema = schema["BatteryDetails"]
                objectSchema?.addField("remainingCapacity", Int::class.javaPrimitiveType!!)
                oldVersion++
            }
            if (oldVersion == 4L) {
                schema.create("SensorDetails")
                    .addField("fifoMaxEventCount", Int::class.javaPrimitiveType!!)
                    .addField("fifoReservedEventCount", Int::class.javaPrimitiveType!!)
                    .addField("highestDirectReportRateLevel", Int::class.javaPrimitiveType!!)
                    .addField("id", Int::class.javaPrimitiveType!!)
                    .addField("isAdditionalInfoSupported", Boolean::class.javaPrimitiveType!!)
                    .addField("isDynamicSensor", Boolean::class.javaPrimitiveType!!)
                    .addField("isWakeUpSensor", Boolean::class.javaPrimitiveType!!)
                    .addField("maxDelay", Int::class.javaPrimitiveType!!)
                    .addField("maximumRange", Float::class.javaPrimitiveType!!)
                    .addField("minDelay", Int::class.javaPrimitiveType!!)
                    .addField("name", String::class.java)
                    .addField("power", Float::class.javaPrimitiveType!!)
                    .addField("reportingMode", Int::class.javaPrimitiveType!!)
                    .addField("resolution", Float::class.javaPrimitiveType!!)
                    .addField("stringType", String::class.java)
                    .addField("codeType", Int::class.javaPrimitiveType!!)
                    .addField("vendor", String::class.java)
                    .addField("version", Int::class.javaPrimitiveType!!)
                objectSchema = schema["SensorDetails"]
                schema["Sample"]?.addRealmListField("sensorDetailsList", objectSchema!!)
                oldVersion++
            }

            if (oldVersion == 5L) {
                objectSchema = schema["SensorDetails"]
                objectSchema?.addField("frequencyOfUse", Int::class.javaPrimitiveType!!)
                    ?.addField("iniTimestamp", Long::class.javaPrimitiveType!!)
                    ?.addField("endTimestamp", Long::class.javaPrimitiveType!!)
            }
        } catch (e: NullPointerException) {
            LogUtils.logE(TAG, "Schema is null!")
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG: String = makeLogTag(BatteryAppDbMigration::class.java)
    }
}
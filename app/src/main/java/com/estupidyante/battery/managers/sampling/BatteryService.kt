package com.estupidyante.battery.managers.sampling

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import com.estupidyante.battery.util.LogUtils.logE
import com.estupidyante.battery.util.LogUtils.makeLogTag
import com.estupidyante.battery.util.SettingsUtils

class BatteryService : Service() {
    private var mIntentFilter: IntentFilter? = null
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        isServiceRunning = true
        estimator = DataEstimator
        mIntentFilter = IntentFilter()
        mIntentFilter!!.addAction(Intent.ACTION_BATTERY_CHANGED)
        val context = applicationContext
        registerReceiver(estimator, mIntentFilter)
        if (SettingsUtils.isSamplingScreenOn(context)) {
            mIntentFilter!!.addAction(Intent.ACTION_SCREEN_ON)
            registerReceiver(estimator, mIntentFilter)
            mIntentFilter!!.addAction(Intent.ACTION_SCREEN_OFF)
            registerReceiver(estimator, mIntentFilter)
        }
    }

    override fun onDestroy() {
        isServiceRunning = false
        try {
            unregisterReceiver(estimator)
        } catch (e: IllegalArgumentException) {
            logE(TAG, "Estimator receiver is not registered!")
            e.printStackTrace()
        }
        estimator = null
    }

    companion object {
        private val TAG = makeLogTag(BatteryService::class.java)
        var isServiceRunning = false
        var estimator: DataEstimator? = null
    }
}
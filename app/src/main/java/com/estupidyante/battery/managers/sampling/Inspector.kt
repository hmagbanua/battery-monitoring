package com.estupidyante.battery.managers.sampling

import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import com.estupidyante.battery.models.Battery
import com.estupidyante.battery.models.Screen
import com.estupidyante.battery.models.data.BatteryDetails
import com.estupidyante.battery.models.data.BatterySession
import com.estupidyante.battery.models.data.BatteryUsage
import com.estupidyante.battery.util.LogUtils.makeLogTag

object Inspector {
    var isSampling = false

    private val TAG: String = makeLogTag(Inspector::class.java)

    private val INSTALLED = "installed:"

    private val REPLACED = "replaced:"

    private val UNINSTALLED = "uninstalled:"

    // Disabled or turned off applications will be scheduled for reporting using this prefix
    private val DISABLED = "disabled:"

    private var sLastBatteryLevel = 0.0

    private var sCurrentBatteryLevel = 0.0

    fun getCurrentBatteryLevel(): Double {
        return sCurrentBatteryLevel
    }

    fun getBatterySession(context: Context?, intent: Intent): BatterySession? {
        val session = BatterySession()
        session.timestamp = System.currentTimeMillis()
        session.id = java.lang.String.valueOf(session.timestamp).hashCode()
        session.level = sCurrentBatteryLevel.toFloat()
        session.screenOn = Screen.isOn(context!!)
        session.triggeredBy = intent.action
        return session
    }

    fun setLastBatteryLevel(level: Double) {
        sLastBatteryLevel = level
    }

    fun getLastBatteryLevel(): Double {
        return sLastBatteryLevel
    }

    fun setCurrentBatteryLevel(
        currentLevel: Double,
        scale: Double
    ) {
        val level = currentLevel / scale
        if (level != sCurrentBatteryLevel) sCurrentBatteryLevel = level
    }

    fun getBatteryUsage(context: Context?, intent: Intent): BatteryUsage? {
        val usage = BatteryUsage()
        val details = BatteryDetails()

        // Battery details
        val health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0)
        val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0)
        val plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0)
        val batteryTechnology =
            intent.extras!!.getString(BatteryManager.EXTRA_TECHNOLOGY)
        var batteryHealth = "Unknown"
        var batteryCharger = "unplugged"
        val batteryStatus: String
        usage.timestamp = System.currentTimeMillis()
        usage.id = java.lang.String.valueOf(usage.timestamp).hashCode()
        when (health) {
            BatteryManager.BATTERY_HEALTH_DEAD -> batteryHealth = "Dead"
            BatteryManager.BATTERY_HEALTH_GOOD -> batteryHealth = "Good"
            BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE -> batteryHealth = "Over voltage"
            BatteryManager.BATTERY_HEALTH_OVERHEAT -> batteryHealth = "Overheat"
            BatteryManager.BATTERY_HEALTH_UNKNOWN -> batteryHealth = "Unknown"
            BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE -> batteryHealth =
                "Unspecified failure"
        }
        batteryStatus = when (status) {
            BatteryManager.BATTERY_STATUS_CHARGING -> "Charging"
            BatteryManager.BATTERY_STATUS_DISCHARGING -> "Discharging"
            BatteryManager.BATTERY_STATUS_FULL -> "Full"
            BatteryManager.BATTERY_STATUS_NOT_CHARGING -> "Not charging"
            BatteryManager.BATTERY_STATUS_UNKNOWN -> "Unknown"
            else -> "Unknown"
        }
        when (plugged) {
            BatteryManager.BATTERY_PLUGGED_AC -> batteryCharger = "ac"
            BatteryManager.BATTERY_PLUGGED_USB -> batteryCharger = "usb"
            BatteryManager.BATTERY_PLUGGED_WIRELESS -> batteryCharger = "wireless"
        }
        details.temperature =
            (intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0).toFloat() / 10).toDouble()

        // current battery voltage in VOLTS
        // (the unit of the returned value by BatteryManager is millivolts)
        details.voltage =
            (intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0).toFloat() / 1000).toDouble()
        details.charger = batteryCharger
        details.health = batteryHealth
        details.technology = batteryTechnology

        // Battery other values with API level limitations
        details.capacity = Battery.getBatteryDesignCapacity(context)
        details.chargeCounter = Battery.getBatteryChargeCounter(context!!)
        details.currentAverage = Battery.getBatteryCurrentAverage(context)
        details.currentNow = Battery.getBatteryCurrentNow(context)
        details.energyCounter = Battery.getBatteryEnergyCounter(context)
        details.remainingCapacity = Battery.getBatteryRemainingCapacity(context)
        usage.level = sCurrentBatteryLevel.toFloat()
        usage.state = batteryStatus
        usage.screenOn = Screen.isOn(context)
        usage.triggeredBy = intent.action
        usage.details = details
        return usage
    }
}
package com.estupidyante.battery.managers.task

import android.os.AsyncTask
import com.estupidyante.battery.models.data.BatteryUsage
import com.estupidyante.battery.util.DateUtils
import io.realm.Realm
import io.realm.RealmResults

open class DeleteUsagesTask :
    AsyncTask<Int?, Void?, Boolean>() {
    private var mResponse = false

    override fun doInBackground(vararg params: Int?): Boolean {
        mResponse = false
        // Open the Realm
        val realm = Realm.getDefaultInstance()
        realm.use { realm ->
            val interval = params[0]
            realm.executeTransaction { realm ->
                val usages: RealmResults<BatteryUsage> =
                    realm.where(BatteryUsage::class.java).lessThan(
                        "timestamp",
                        DateUtils.getMilliSecondsInterval(interval!!)
                    ).findAll()
                if (usages != null && !usages.isEmpty()) {
                    mResponse = usages.deleteAllFromRealm()
                }
            }
        }
        return mResponse
    }
}
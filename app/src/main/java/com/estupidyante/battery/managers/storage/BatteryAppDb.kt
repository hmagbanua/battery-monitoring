package com.estupidyante.battery.managers.storage

import android.content.Intent
import com.estupidyante.battery.models.data.BatterySession
import com.estupidyante.battery.models.data.BatteryUsage
import com.estupidyante.battery.models.data.Message
import com.estupidyante.battery.models.data.Sample
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import io.realm.exceptions.RealmMigrationNeededException
import java.util.*

class BatteryAppDb {
    private var mRealm: Realm? = null
    val defaultInstance: Unit
        get() {
            if (mRealm!!.isClosed) {
                mRealm = Realm.getDefaultInstance()
            }
        }

    fun close() {
        mRealm!!.close()
    }

    val isClosed: Boolean
        get() = mRealm!!.isClosed

    fun count(className: Class<*>): Long {
        var size: Long = -1
        when (className) {
            Sample::class.java -> {
                size = mRealm!!.where(Sample::class.java).count()
            }
            BatteryUsage::class.java -> {
                size = mRealm!!.where(BatteryUsage::class.java).count()
            }
            BatterySession::class.java -> {
                size = mRealm!!.where(BatterySession::class.java).count()
            }
            Message::class.java -> {
                size = mRealm!!.where(Message::class.java).count()
            }
        }
        return size
    }

    fun lastSample(): Sample? {
        return if (mRealm!!.where(Sample::class.java).count() > 0) {
            mRealm!!.where(Sample::class.java).findAll().last()
        } else null
    }

    /**
     * Store the sample into the database
     *
     * @param sample the sample to be saved
     */
    fun saveSample(sample: Sample?) {
        mRealm!!.beginTransaction()
        mRealm!!.copyToRealm(sample!!)
        mRealm!!.commitTransaction()
    }

    /**
     * Store the usage details into the database
     *
     * @param usage the usage details to be saved
     */
    fun saveUsage(usage: BatteryUsage?) {
        mRealm!!.beginTransaction()
        mRealm!!.copyToRealm(usage!!)
        mRealm!!.commitTransaction()
    }

    /**
     * Store a new battery session into the database
     *
     * @param session the session to be saved
     */
    fun saveSession(session: BatterySession?) {
        mRealm!!.beginTransaction()
        mRealm!!.copyToRealm(session!!)
        mRealm!!.commitTransaction()
    }

    fun allSamplesIds(): Iterator<Int> {
        val list = ArrayList<Int>()
        val samples = mRealm!!.where(Sample::class.java).findAll()
        if (!samples.isEmpty()) {
            for (sample in samples) {
                list.add(sample.id)
            }
        }
        return list.iterator()
    }

    fun betweenUsages(from: Long, to: Long): RealmResults<BatteryUsage> {
        return mRealm!!
            .where(BatteryUsage::class.java)
            .equalTo("triggeredBy", Intent.ACTION_BATTERY_CHANGED)
            .between("timestamp", from, to)
            .sort("timestamp")
            .findAll()
    }

    fun allMessages(): RealmResults<Message> {
        return mRealm!!
            .where(Message::class.java)
            .sort("id", Sort.DESCENDING)
            .findAll()
    }

    val usages: RealmResults<BatteryUsage>
        get() = mRealm!!
            .where(BatteryUsage::class.java)
            .sort("timestamp", Sort.DESCENDING)
            .findAll()

    fun markMessageAsRead(id: Int) {
        mRealm!!.beginTransaction()
        val message =
            mRealm!!.where(Message::class.java).equalTo("id", id).findFirst()
        if (message != null) message.read = true
        mRealm!!.commitTransaction()
    }

    fun deleteMessage(id: Int) {
        mRealm!!.beginTransaction()
        val message =
            mRealm!!.where(Message::class.java).equalTo("id", id).findFirst()
        message?.deleteFromRealm()
        mRealm!!.commitTransaction()
    }

    init {
        try {
            mRealm = Realm.getDefaultInstance()
        } catch (e: RealmMigrationNeededException) {
            // handle migration exception io.realm.exceptions.RealmMigrationNeededException
            e.printStackTrace()
        }
    }
}
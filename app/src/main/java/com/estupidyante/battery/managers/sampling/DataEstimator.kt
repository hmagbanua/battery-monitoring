package com.estupidyante.battery.managers.sampling

import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.*
import android.os.BatteryManager
import com.estupidyante.battery.R
import com.estupidyante.battery.events.BatteryLevelEvent
import com.estupidyante.battery.util.LogUtils.logE
import com.estupidyante.battery.util.LogUtils.logI
import com.estupidyante.battery.util.LogUtils.makeLogTag
import com.estupidyante.battery.util.Notifier
import com.estupidyante.battery.util.SettingsUtils
import org.greenrobot.eventbus.EventBus
import java.util.*

object DataEstimator : BroadcastReceiver() {

    private val TAG: String = makeLogTag(DataEstimator::class.java)

    private var mJobId = 0
    private var mContext: Context? = null
    private var mIntent: Intent? = null
    private var mAction: String? = null

    private var mLastNotify: Long = 0
    private var mHealth = 0
    private var mLevel = 0
    private var mPlugged = 0
    private var mPresent = false
    private var mScale = 0
    private var mStatus = 0
    private var mTechnology: String? = null
    private var mTemperature = 0f
    private var mVoltage = 0f

    override fun onReceive(context: Context, intent: Intent) {
        mContext = context
        mIntent = intent
        mAction = intent.action

        when (mAction) {
            null -> {
                logE(TAG, "Intent has no action")
                return
            }

            else -> {
                logI(TAG, "ENTRY onReceive => $mAction")
                if (mAction != Intent.ACTION_BATTERY_CHANGED) return

                // Fetch Intent extras related to the battery state

                // Fetch Intent extras related to the battery state
                try {
                    mLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
                    mScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
                    mHealth = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0)
                    mPlugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0)
                    mPresent = intent.extras!!.getBoolean(BatteryManager.EXTRA_PRESENT)
                    mStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0)
                    mTechnology = intent.extras!!.getString(BatteryManager.EXTRA_TECHNOLOGY)
                    mTemperature =
                        intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0).toFloat() / 10
                    mVoltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0).toFloat() / 1000
                } catch (e: RuntimeException) {
                    e.printStackTrace()
                }

                if (mTemperature > SettingsUtils.fetchTemperatureWarning(
                        context
                    )
                ) {
                    if (SettingsUtils.isBatteryAlertsOn(context) &&
                        SettingsUtils.isTemperatureAlertsOn(context)
                    ) {

                        // Check mTemperature limit rate
                        val lastAlert = Calendar.getInstance()
                        val lastSavedTime =
                            SettingsUtils.fetchLastTemperatureAlertDate(context)

                        // Set last alert time with saved preferences
                        if (lastSavedTime != 0L) {
                            lastAlert.timeInMillis = lastSavedTime
                        }
                        val minutes = SettingsUtils.fetchTemperatureAlertsRate(context)
                        lastAlert.add(Calendar.MINUTE, minutes)

                        // If last saved time isn't default and now is after limit rate then notify
                        if (lastSavedTime == 0L || Calendar.getInstance().after(lastAlert)) {
                            SettingsUtils.saveLastTemperatureAlertDate(
                                context,
                                System.currentTimeMillis()
                            )
                        }
                    }
                }

                if (SettingsUtils.isPowerIndicatorShown(context)) {
                    logI(TAG, "Updating notification mStatus bar")
                    Notifier.updateStatusBar(context)
                }

                // On some phones, mScale is always 0.
                if (mScale == 0) mScale = 100

                if (mLevel > 0) {
                    Inspector.setCurrentBatteryLevel(mLevel.toDouble(), mScale.toDouble())
                    EventBus.getDefault().post(BatteryLevelEvent(mLevel))
                    scheduleJob(context)
                }
            }
        }

    }

    // Getters & Setters
    fun getCurrentStatus(context: Context) {
        val ifilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        try {
            val batteryStatus = context.registerReceiver(null, ifilter)
            if (batteryStatus != null) {
                mLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
                mScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
                mHealth = batteryStatus.getIntExtra(BatteryManager.EXTRA_HEALTH, 0)
                mPlugged = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0)
                mPresent = batteryStatus.extras!!.getBoolean(BatteryManager.EXTRA_PRESENT)
                mStatus = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, 0)
                mTechnology = batteryStatus.extras!!.getString(BatteryManager.EXTRA_TECHNOLOGY)
                mTemperature = (batteryStatus.getIntExtra(
                    BatteryManager.EXTRA_TEMPERATURE, 0
                ) / 10).toFloat()
                mVoltage = (batteryStatus.getIntExtra(
                    BatteryManager.EXTRA_VOLTAGE, 0
                ) / 1000).toFloat()
            }
        } catch (e: ReceiverCallNotAllowedException) {
            logE(TAG, "ReceiverCallNotAllowedException from Notification Receiver?")
            e.printStackTrace()
        }
    }

    fun getHealthStatus(): String? {
        var status = ""
        when (mHealth) {
            BatteryManager.BATTERY_HEALTH_UNKNOWN -> status = "Unknown"
            BatteryManager.BATTERY_HEALTH_GOOD -> status = "Good"
            BatteryManager.BATTERY_HEALTH_OVERHEAT -> status = "Overheat"
            BatteryManager.BATTERY_HEALTH_DEAD -> status = "Dead"
            BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE -> status = "Over Voltage"
            BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE -> status = "Unspecified Failure"
        }
        return status
    }

    fun getHealthStatus(context: Context): String? {
        var status = ""
        when (mHealth) {
            BatteryManager.BATTERY_HEALTH_UNKNOWN -> status =
                context.getString(R.string.battery_health_unknown)
            BatteryManager.BATTERY_HEALTH_GOOD -> status =
                context.getString(R.string.battery_health_good)
            BatteryManager.BATTERY_HEALTH_OVERHEAT -> status =
                context.getString(R.string.battery_health_overheat)
            BatteryManager.BATTERY_HEALTH_DEAD -> status =
                context.getString(R.string.battery_health_dead)
            BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE -> status =
                context.getString(R.string.battery_health_over_voltage)
            BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE -> status =
                context.getString(R.string.battery_health_failure)
        }
        return status
    }

    fun getBatteryChangedIntent(context: Context): Intent? {
        return context.registerReceiver(
            null, IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        )
    }

    fun getmLastNotify(): Long {
        return mLastNotify
    }

    fun setmLastNotify(now: Long) {
        mLastNotify = now
    }

    fun getHealth(): Int {
        return mHealth
    }

    fun getLevel(): Int {
        return mLevel
    }

    fun getPlugged(): Int {
        return mPlugged
    }

    fun isPresent(): Boolean {
        return mPresent
    }

    fun getScale(): Int {
        return mScale
    }

    fun getStatus(): Int {
        return mStatus
    }

    fun getTechnology(): String? {
        return mTechnology
    }

    fun getTemperature(): Float {
        return mTemperature
    }

    fun getVoltage(): Float {
        return mVoltage
    }

    private fun scheduleJob(context: Context) {
        val serviceComponent = ComponentName(context, EstimatorJob::class.java)
        val builder = JobInfo.Builder(DataEstimator.mJobId++, serviceComponent)
        builder.setMinimumLatency(1) // wait at least
        builder.setOverrideDeadline(10) // maximum delay
        builder.setRequiresCharging(false)
        builder.setRequiresDeviceIdle(false)
        val jobScheduler =
            context.getSystemService(JobScheduler::class.java)
        jobScheduler?.schedule(builder.build())
    }

    class EstimatorJob : JobService() {
        override fun onStartJob(params: JobParameters): Boolean {
            val service = Intent(mContext, DataEstimatorService::class.java)
            service.putExtra("OriginalAction", mAction)
            service.fillIn(mIntent!!, 0)
            startService(service)
            jobFinished(params, false)
            return true
        }

        override fun onStopJob(params: JobParameters): Boolean {
            return false
        }

        companion object {
            const val TAG = "EstimatorJob"
        }
    }
}
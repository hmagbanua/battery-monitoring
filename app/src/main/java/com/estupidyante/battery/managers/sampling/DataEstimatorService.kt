package com.estupidyante.battery.managers.sampling

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import android.os.Bundle
import com.estupidyante.battery.Config
import com.estupidyante.battery.R
import com.estupidyante.battery.events.StatusEvent
import com.estupidyante.battery.managers.storage.BatteryAppDb
import com.estupidyante.battery.models.data.BatteryUsage
import com.estupidyante.battery.models.data.Sample
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.logI
import com.estupidyante.battery.util.LogUtils.makeLogTag
import com.estupidyante.battery.util.Notifier
import com.estupidyante.battery.util.SettingsUtils
import org.greenrobot.eventbus.EventBus

class DataEstimatorService : IntentService(TAG) {
    override fun onHandleIntent(intent: Intent?) {
        val context = applicationContext
        intent?.let { takeSampleIfBatteryLevelChanged(context, it) }
    }

    private fun takeSampleIfBatteryLevelChanged(
        context: Context,
        intent: Intent
    ) {
        val action = intent.action ?: return

        // Make sure our new sample doesn't have a zero value as its current battery level
        if (Inspector.getCurrentBatteryLevel() <= 0) return
        val database = BatteryAppDb()
        val lastSample: Sample? = database.lastSample()

        // If intent has action Screen ON or Screen OFF don't check the change on battery level
        if (action == Intent.ACTION_SCREEN_ON || action == Intent.ACTION_SCREEN_OFF) {
            LogUtils.logI(TAG, "Getting new usage details")
            getBatteryUsage(context, intent, database, true)
            database.close()
            return
        }

        // Set last sample, if exists extract the last battery level
        if (lastSample != null) {
            Inspector.setLastBatteryLevel(lastSample.batteryLevel)
        }

        /*
         * Read the battery levels again, they are now changed. We just
         * changed the last battery level (in the previous block of code).
         * The current battery level might also have been changed while the
         * device has been taking a sample.
         */
        val batteryLevelChanged =
            Inspector.getLastBatteryLevel() !== Inspector.getCurrentBatteryLevel()

        /*
         * Among all occurrence of the event BATTERY_CHANGED, only take a sample
         * whenever a battery PERCENTAGE CHANGE happens
         * (BATTERY_CHANGED happens whenever the battery temperature
         * or voltage of other parameters change)
         */if (!batteryLevelChanged || Inspector.isSampling) {
            if (!batteryLevelChanged) {
                logI(
                    TAG,
                    "No battery percentage change. BatteryLevel=" +
                            Inspector.getCurrentBatteryLevel()
                )
            } else if (Inspector.isSampling) {
                logI(TAG, "Inspector is already sampling...")
            }
        } else {
            val message = "The battery percentage changed. " +
                    "About to take a new sample (currentBatteryLevel=" +
                    Inspector.getCurrentBatteryLevel() + ", lastBatteryLevel=" +
                    Inspector.getLastBatteryLevel() + ")"
            LogUtils.logI(TAG, message)

            // take a sample and store it in the mDatabase
            EventBus.getDefault()
                .post(StatusEvent(getString(R.string.event_new_sample)))
            getSample(context)
            getBatteryUsage(context, intent, database, false)

            // If last battery level = 0 then it is the first sample in the current instance
            if (Inspector.getLastBatteryLevel() === 0.0) {
                LogUtils.logI(
                    TAG,
                    "Last Battery Level = 0. Updating to BatteryLevel => " +
                            Inspector.getCurrentBatteryLevel()
                )
                // before taking the first sample in a batch, first record the battery level
                Inspector.setLastBatteryLevel(Inspector.getCurrentBatteryLevel())
            }
        }

        // Finally close mDatabase access
        database.close()
    }

    /**
     * Takes a Sample and stores it in the mDatabase. Does not store the first ever samples
     * that have no battery info.
     *
     * @param context  from onReceive
     * @param intent   from onReceive
     * @param database GreenHub database
     */
    private fun getSample(
        context: Context
    ) {
        // Notify UI
        EventBus.getDefault()
            .post(StatusEvent(context.getString(R.string.event_idle)))
    }

    private fun getBatteryUsage(
        context: Context, intent: Intent, database: BatteryAppDb,
        isScreenIntent: Boolean
    ) {
        // if Intent is screen related, it is necessary to add extras from DataEstimator
        // since original intent has none
        if (isScreenIntent) {
            val extras: Bundle? = DataEstimator.getBatteryChangedIntent(context)?.extras
            if (extras != null) intent.putExtras(extras)
        }
        val usage: BatteryUsage? = Inspector.getBatteryUsage(context, intent)
        if (usage != null && !usage.state.equals("Unknown") && usage.level >= 0) {
            database.saveUsage(usage)
            LogUtils.logI(
                TAG,
                "Took usage details " + usage.id.toString() + " for " + intent.action
            )
        }
    }

    companion object {
        private val TAG: String = makeLogTag(DataEstimatorService::class.java)
    }
}
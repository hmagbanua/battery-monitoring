package com.estupidyante.battery

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import com.crashlytics.android.Crashlytics
import com.estupidyante.battery.managers.sampling.BatteryService
import com.estupidyante.battery.managers.sampling.DataEstimator
import com.estupidyante.battery.managers.storage.BatteryAppDbMigration
import com.estupidyante.battery.managers.task.DeleteSessionsTask
import com.estupidyante.battery.managers.task.DeleteUsagesTask
import com.estupidyante.battery.receivers.NotificationReceiver
import com.estupidyante.battery.util.LogUtils
import com.estupidyante.battery.util.LogUtils.logI
import com.estupidyante.battery.util.LogUtils.makeLogTag
import com.estupidyante.battery.util.SettingsUtils
import io.fabric.sdk.android.BuildConfig
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration

class BatteryApp : Application() {
    private var mAlarmManager: AlarmManager? = null
    private var mNotificationIntent: PendingIntent? = null

    override fun onCreate() {
        super.onCreate()

        // If running debug mode, enable logs
        if (BuildConfig.DEBUG) {
            LogUtils.LOGGING_ENABLED = true
        }

        logI(TAG, "onCreate() called")

        // Init crash reports
        Fabric.with(this, Crashlytics())

        // Database init
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .schemaVersion(Config.DATABASE_VERSION.toLong())
            .migration(BatteryAppDbMigration())
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)
        logI(TAG, "Estimator new instance")

        context = applicationContext

        // Start Service
        logI(TAG, "startService() called")
        startBatteryService()

        // Delete old data history
        val interval = SettingsUtils.fetchDataHistoryInterval(context)
        DeleteUsagesTask().execute(interval)
        DeleteSessionsTask().execute(interval)
        if (SettingsUtils.isPowerIndicatorShown(context)) {
            startStatusBarUpdater()
        }
    }

    fun startBatteryService() {
        if (!BatteryService.isServiceRunning) {
            logI(TAG, "GreenHubService starting...")

            context = getAppContext()

            object : Thread() {
                override fun run() {
                    try {
                        val service = Intent(context, BatteryService::class.java)
                        context?.startService(service)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
                }
            }.start()
        } else {
            logI(TAG, "GreenHubService is already running...")
        }
    }

    fun stopGreenHubService() {
        val service = Intent(applicationContext, BatteryService::class.java)
        stopService(service)
    }

    val estimator: DataEstimator?
        get() = BatteryService.estimator

    fun startStatusBarUpdater() {
        val notificationIntent = Intent(this, NotificationReceiver::class.java)
        mNotificationIntent = PendingIntent.getBroadcast(
            this,
            0,
            notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        if (mAlarmManager == null) {
            mAlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        }
        mAlarmManager!!.setInexactRepeating(
            AlarmManager.ELAPSED_REALTIME,
            SystemClock.elapsedRealtime() + Config.REFRESH_STATUS_BAR_INTERVAL,
            Config.REFRESH_STATUS_BAR_INTERVAL,
            mNotificationIntent
        )
    }

    fun stopStatusBarUpdater() {
        if (mAlarmManager != null) {
            mAlarmManager!!.cancel(mNotificationIntent)
        }
    }

    private fun getAppContext(): Context? {
        return context
    }

    companion object {
        private val TAG = makeLogTag(BatteryApp::class.java)
        private var context: Context? = null
    }
}
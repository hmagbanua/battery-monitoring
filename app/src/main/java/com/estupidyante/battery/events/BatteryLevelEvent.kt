package com.estupidyante.battery.events

class BatteryLevelEvent(val level: Int)
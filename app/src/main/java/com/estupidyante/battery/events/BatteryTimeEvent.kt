package com.estupidyante.battery.events

class BatteryTimeEvent(
    val remainingHours: Int,
    val remainingMinutes: Int,
    val charging: Boolean
)